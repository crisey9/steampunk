﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Graphic.Particles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ParticleEditor
{
    class Editor : GraphicsDeviceControl
    {
        ContentManager Content;
        private SpriteBatch spriteBatch;
        private Texture2D texture;
        private ParticleEngine engine;

        public Editor()
        {
            Content = new ContentManager(this.Services);
            Content.RootDirectory = "Content";
        }
        
        protected override void Draw()
        {
            
            Console.WriteLine("a");
          //  Draw();
        }

        protected override void Initialize()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Content.Load<Texture2D>("snow"));
            textures.Add(Content.Load<Texture2D>("snow1"));

            engine = new ParticleEngine(textures, new Vector2(50,300));
            engine.HeightRespawn = 275;
            engine.WidthRespawn = 65;
            engine.MinParticles = 20;
            engine.MaxParticles = 25;
            engine.TimeLife = 500;
            engine.EmitterLocation = new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
            engine.Util = new ParticleEngineUtil(600, 60, 105, 7, 30, -4, 4, -135, 140, 310, 660);
        }
    }
}
