﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphic
{
    public class BaseDrawableGameComponent : DrawableGameComponent
    {
       // private Game game;
        protected ContentManager Content;
        public BaseDrawableGameComponent(Game game) : base(game)
        {
            Content = new ContentManager(Game.Services);
            Content.RootDirectory = "Content";
        }
    }
}
