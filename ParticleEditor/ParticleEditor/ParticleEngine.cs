﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GameLibrary.Graphic.Particles;
using GameLibrary.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphic.Particles
{
    public class ParticleEngine
    {
        private ThreadSafeRandom rnd;
        public Vector2 EmitterLocation { get; set; }
        private List<Particle> particles;
        private List<Texture2D> textures;

        public ParticleEngineUtil Util { get; set; }
        public int WidthRespawn { get; set; }
        public int HeightRespawn { get; set; }
        public int MinParticles { get; set; }
        public int MaxParticles { get; set; }
        public int TimeLife { get; set; }

        public ParticleEngine(List<Texture2D> textures, Vector2 location)
        {
            EmitterLocation = location;
            this.textures = textures;
            this.particles = new List<Particle>();
            rnd = new ThreadSafeRandom();
        }

        public void Update()
        {
            int count = rnd.Next(
                MinParticles - particles.Count,
                MaxParticles - particles.Count);

            for (int i = 0; i < count; i++)
            {
                particles.Add(GenerateNewParticle());
            }

            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].TimeLife <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        private Particle GenerateNewParticle()
        {
            Texture2D texture = textures[rnd.Next(textures.Count)];
            Vector2 position = new Vector2(
                EmitterLocation.X + (float)rnd.NextDouble(-(WidthRespawn / 2), WidthRespawn / 2),
                EmitterLocation.Y + (float)rnd.NextDouble(-(HeightRespawn / 2), HeightRespawn / 2));

            Vector2 velocity = new Vector2((float)rnd.Next(Util.VelocityXMinRnd, Util.VelocityXMaxRnd) / 100,
                (float)rnd.Next(Util.VelocityYMinRnd, Util.VelocityYMaxRnd) / 100);

            float angle = (float)rnd.NextDouble(Util.AngleMinRnd, Util.AngleMaxRnd) / 100;
            float angularVelocity = 0.05f + (float)rnd.NextDouble(Util.AngularVelocityMinRnd, Util.AngularVelocityMaxRnd) / 100;
            Color color = Color.White;
            float size = (float)rnd.NextDouble(Util.SizeMinRnd, Util.SizeMaxRnd) / 100;
            int timelife = TimeLife + rnd.Next(Util.TimeLifeRnd);

            if (velocity.X > 0)
            {
                angle = -angle;
            }

            return new Particle(texture, position, velocity, angle, angularVelocity, color, size, timelife);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Texture, BlendState.AlphaBlend);
            for (int index = 0; index < particles.Count; index++)
            {
                particles[index].Draw(spriteBatch);
            }
            spriteBatch.End();
        }
    }
}
