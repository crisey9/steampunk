﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphic.Particles
{
    public class Particle
    {
        public Texture2D Texture { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; } = new Vector2(0, 0);
        public float Angle { get; set; }
        public float AngularVelocity { get; set; }
        public Color Color { get; set; }
        public float Size { get; set; }
        public int TimeLife { get; set; }
        private ThreadSafeRandom rnd;
        private int elapsed = 0;
        private Stopwatch stop = null;

        public Particle(Texture2D texture, Vector2 position, Vector2 velocity,
            float angle, float angularVelocity, Color color, float size, int ttl)
        {
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            Color = color;
            Size = size;
            TimeLife = ttl;
            rnd = new ThreadSafeRandom();
        }

        public void Update()
        {
            if (stop != null)
            {
                stop.Stop();
                elapsed = (int) stop.ElapsedMilliseconds;
            }
            else
            {
                stop = new Stopwatch();
            }
            stop.Start();
            TimeLife -= elapsed;
            if (TimeLife <= 195)
            {
                Color *= 0.8825f;
            }

            if (Color.A <= 50)
            {
                TimeLife = 0;
            }
            // Console.WriteLine(transparency * 100f);

            if (Velocity.Y >= 1.3f + rnd.NextDouble(-17, 14) / 100)
            {

                Velocity = new Vector2(Velocity.X, Velocity.Y - (0.0375f + (float)rnd.NextDouble(-7, 18) / 1000));
            }
            Position += Velocity;
            Angle += AngularVelocity;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Rectangle sourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);
            Vector2 origin = new Vector2(Texture.Width / 2, Texture.Height / 2);

            spriteBatch.Draw(Texture, Position, sourceRectangle, Color,
                Angle, origin, Size, SpriteEffects.None, 0f);
        }
    }
}
