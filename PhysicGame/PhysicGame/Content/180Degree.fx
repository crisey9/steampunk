﻿sampler s0;

float4 PixelShaderFunction(float4 color : COLOR0, float4 pos : POSITION0, float2 coords : TEXCOORD0) : SV_Position0
{
    coords = float2(coords.x - 1, coords.y);
    color = (s0, coords);
    return color;
}

technique BasicColorDrawing
{
    pass P0
    {
        PixelShader = compile ps_4_0 PixelShaderFunction();
    }
};