﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Map;
using GameLibrary.Map.RogueLike;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PhysicGame.Objects;

namespace PhysicGame
{
    class SpawnMonsters : DrawableGameComponent
    {
        private List<CannonMonster> monsters;
        private Camera _camera;


        public SpawnMonsters(Game game, CorporationGenerator generator, Map map, Camera camera, Player player, int monsterCountMin, int monsterCountMax) : base(game)
        {
            _camera = camera;
            var size = generator.CellSize;
            monsters = new List<CannonMonster>();
            var rnd = new Random();

            foreach (var room in generator.Corridors)
            {
                var monsterCount = rnd.Next(monsterCountMin, monsterCountMax);
                for (int i = 0; i < monsterCount; i++)
                {
                    CannonMonster cannonMonster = new CannonMonster(game, map, camera, player);
                    cannonMonster.Position = new Position(rnd.Next(Math.Min(room.Bounds.Left * size, room.Bounds.Right * size) + cannonMonster.Width + 10, Math.Max(room.Bounds.Left * size, room.Bounds.Right * size) - cannonMonster.Width - 10),
                        rnd.Next(Math.Min(room.Bounds.Top * size, room.Bounds.Bottom * size) + cannonMonster.Height + 10, Math.Max(room.Bounds.Top * size, room.Bounds.Bottom * size) - cannonMonster.Height - 10));
                    monsters.Add(cannonMonster);
                    Console.WriteLine(cannonMonster.Position);
                }
            }
        }

        public void LoadContent()
        {

            base.LoadContent();
            foreach (var monster in monsters)
            {
                monster.LoadContent();
                monster.Initialize();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var monster in monsters)
            {
                monster.Draw(spriteBatch);
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var monster in monsters)
            {
                monster.Update(gameTime);
            }
            base.Update(gameTime);
        }
    }
}
