﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhysicGame.Menu;

namespace PhysicGame
{
    public static class GlobalVar
    {
        public static MenuManager.GameState GameState;
        public static Options.OptionsState OptionsState;
    }
}
