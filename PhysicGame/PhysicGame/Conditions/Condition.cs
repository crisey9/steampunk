﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PhysicGame.Conditions
{
    public class Condition : DrawableGameComponent
    {
        private Effect effect;
        public Condition(Game game) : base(game)
        {

        }

        private float _value = 50;
        private float _valueMin = 0;
        private float _valueMax = 100;
        private Texture2D _texture;
        private Texture2D _textureEmpty;
        private Rectangle _bounds;

        public event EventHandler ValueChanged;
        public event EventHandler BoundsChanged;
        public bool isHorizontal { get; set; } = true;

        public float ValuePercentage => (Value - ValueMin) / (ValueMax - ValueMin) * 100;

        public float ValueMax
        {
            get { return _valueMax; }
            set
            {
                _valueMax = value;
            }
        }

        public float ValueMin
        {
            get { return _valueMin; }
            set { _valueMin = value; }
        }

        public float Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnValueChanged(EventArgs.Empty);
            }
        }

        public Texture2D Texture
        {
            get { return _texture ?? Game.Content.Load<Texture2D>("Textures/Default"); }
            set { _texture = value; }
        }

        public Texture2D TextureEmpty
        {
            get { return _textureEmpty ?? Game.Content.Load<Texture2D>("Textures/Default"); }
            set { _textureEmpty = value; }
        }


        public Rectangle Bounds
        {
            get { return _bounds; }
            set
            {
                _bounds = value;
                OnBoundsChanged(EventArgs.Empty);
            }
        }

        #region events
        protected virtual void OnValueChanged(EventArgs e)
        {
            EventHandler eh = ValueChanged;
            if (eh != null)
            {
                eh(this, e);
            }
        }

        protected virtual void OnBoundsChanged(EventArgs e)
        {
            EventHandler eh = BoundsChanged;
            if (eh != null)
            {
                eh(this, e);
            }
        }
        #endregion events

        public void LoadContent()
        {
            effect = Game.Content.Load<Effect>("Shaders/Conditions");
            Texture = Game.Content.Load<Texture2D>("Textures/Hp");
            TextureEmpty = Game.Content.Load<Texture2D>("Textures/HpEmpty");
            base.LoadContent();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(TextureEmpty, Bounds, Color.White);
            spriteBatch.End();
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            effect.Parameters["horizontal"].SetValue(isHorizontal);
            effect.Parameters["value"].SetValue(ValuePercentage/100);
            effect.CurrentTechnique.Passes[0].Apply();
            spriteBatch.Draw(Texture, Bounds, Color.White);
            spriteBatch.End();
        }
    }
}
