﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PhysicGame.Menu
{
    public abstract class MenuCore : DrawableGameComponent
    {
        public List<Control> Controls { get; set; }
        protected Point centerBtnPos;

        public MenuCore(Game game) : base(game)
        {
            Controls = new List<Control>();

        }

        public override void Initialize()
        {
            centerBtnPos = new Point(GraphicsDevice.Viewport.Bounds.Center.X, GraphicsDevice.Viewport.Bounds.Center.Y - GraphicsDevice.Viewport.Height / 4);

            base.Initialize();
        }

        protected override void LoadContent()
        {

            base.LoadContent();
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (var control in Controls)
            {
                control.Draw(spriteBatch);
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (var control in Controls)
            {
                control.Update(gameTime);
            }

            base.Update(gameTime);
        }
    }
}
