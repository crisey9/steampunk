﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PhysicGame.Menu;

namespace PhysicGame
{
    public class MenuManager : DrawableGameComponent
    {
        

        private MainMenu mainMenuDesign;
        private Options optionsDesign;
       
        public MenuManager(Game game) : base(game)
        {
            mainMenuDesign = new MainMenu(game);
            optionsDesign = new Options(game);
        }

        public void LoadContent()
        {
            base.LoadContent();

            mainMenuDesign.LoadContent();
            optionsDesign.LoadContent();
        }

        public override void Initialize()
        {
            base.Initialize();
        
            mainMenuDesign.Initialize();
            optionsDesign.Initialize();

            mainMenuDesign.SetDesign();
            optionsDesign.SetDesign();
        }


        #region Designs
        #region InGame
        private void CreateInGameDesign()
        {

        }

        #endregion InGame



  

        #endregion Designs

        public void Draw(SpriteBatch spriteBatch)
        {
            if(GlobalVar.GameState==GameState.MainMenu)
                mainMenuDesign.Draw(spriteBatch);
            else if (GlobalVar.GameState == GameState.Options)
                optionsDesign.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            if (GlobalVar.GameState == GameState.MainMenu)
                mainMenuDesign.Update(gameTime);
            else if (GlobalVar.GameState == GameState.Options)
                optionsDesign.Update(gameTime);

            base.Update(gameTime);
        }

        public enum GameState
        {
            Null,
            MainMenu,
            InGame,
            Options,
            Authors,
        }

        #region Methouds

        #endregion
    }
}
