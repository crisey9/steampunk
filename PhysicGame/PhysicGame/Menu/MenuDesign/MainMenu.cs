﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Controls;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PhysicGame.Menu
{
    class MainMenu : MenuCore
    {
        private Button playBtn;
        private Button optionsBtn;
        private Button authorsBtn;
        private Button exitBtn;

        public MainMenu(Game game) : base(game)
        {
            playBtn = new Button(game);
            optionsBtn = new Button(game);
            authorsBtn = new Button(game);
            exitBtn = new Button(game);
        }

        public void LoadContent()
        {
            playBtn.LoadContent();
            optionsBtn.LoadContent();
            authorsBtn.LoadContent();
            exitBtn.LoadContent();

            base.LoadContent();
        }

        public void Initialize()
        {
            playBtn.Initialize();
            optionsBtn.Initialize();
            authorsBtn.Initialize();
            exitBtn.Initialize();

            base.Initialize();
        }

        public void SetDesign()
        {
            Controls = new List<Control>();
            int width = 350;

           playBtn.Text.Value = "Play!";
            playBtn.Bounds = new Rectangle(centerBtnPos.X - (width / 2), centerBtnPos.Y, width, 60);
            playBtn.MouseUp += (sender, args) => GlobalVar.GameState = MenuManager.GameState.InGame;

            optionsBtn.Text.Value = "Options";
            optionsBtn.Bounds = new Rectangle(centerBtnPos.X - (width / 2), centerBtnPos.Y + 80, width, 60);
            optionsBtn.MouseUp += (sender, args) => GlobalVar.GameState = MenuManager.GameState.Options;

            authorsBtn.Text.Value = "Authors";
            authorsBtn.Bounds = new Rectangle(centerBtnPos.X - (width / 2), centerBtnPos.Y + 160, width, 60);
            authorsBtn.MouseUp += (sender, args) => GlobalVar.GameState = MenuManager.GameState.Options;

            exitBtn.Text.Value = "Exit";
            exitBtn.Bounds = new Rectangle(centerBtnPos.X - (width / 2), centerBtnPos.Y + 240, width, 60);
            exitBtn.MouseUp += (sender, args) => Game.Exit();


            Controls.Add(authorsBtn);
            Controls.Add(optionsBtn);
            Controls.Add(playBtn);
            Controls.Add(exitBtn);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        private void ExitBtn_MouseUp()
        {
            Game.Exit();
        }
    }
}
