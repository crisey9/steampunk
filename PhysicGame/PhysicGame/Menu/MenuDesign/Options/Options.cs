﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Controls;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PhysicGame.Menu.MenuDesign.Options;

namespace PhysicGame.Menu
{
    public class Options : MenuCore
    {
        private Graphic graphicDesign;

        private RadioArea group1;

        private Radiobox graphicsBtn;
        private Radiobox controlsBtn;
        private Radiobox soundBtn;
        private Radiobox cameraBtn;
        private Button backBtn;

        public Options(Game game) : base(game)
        {
            graphicDesign = new Graphic(game, this);

            graphicsBtn = new Radiobox(game);
            controlsBtn = new Radiobox(game);
            soundBtn = new Radiobox(game);
            cameraBtn = new Radiobox(game);

            group1 = new RadioArea(game);
            backBtn = new Button(game);
        }

        public void LoadContent()
        {
            graphicsBtn.TextIsInCenter = true;
            controlsBtn.TextIsInCenter = true;
            soundBtn.TextIsInCenter = true;
            cameraBtn.TextIsInCenter = true;

            loadtextures(graphicsBtn);
            loadtextures(controlsBtn);
            loadtextures(soundBtn);
            loadtextures(cameraBtn);

            // graphicsBtn.Texture = Game.Content.Load<Texture2D>("Textures/UI/Button");


            graphicDesign.LoadContent();

            base.LoadContent();
        }

        private void loadtextures(Radiobox control)
        {
            control.LoadContent();

            control.UncheckedTexture = Game.Content.Load<Texture2D>("Textures/UI/Button");
            control.CheckedTexture = Game.Content.Load<Texture2D>("Textures/UI/ButtonClicked");

            control.IsChecked = false;
            control.Margin = 75;
        }

        public void Initialize()
        {
            graphicsBtn.Initialize();
            controlsBtn.Initialize();
            soundBtn.Initialize();
            cameraBtn.Initialize();

            graphicDesign.Initialize();
            graphicDesign.SetDesign();

            base.Initialize();
        }

        public void SetDesign()
        {
            Controls = new List<Control>();
            int width = 200;
            var gameViewport = Game.GraphicsDevice.Viewport.Bounds;
            Console.WriteLine(gameViewport.Height / 2);
            gameViewport.Inflate(0, -(gameViewport.Height / 2));

            graphicsBtn.Text.Value = "Graphic";
            graphicsBtn.Bounds = new Rectangle(gameViewport.Left + 25, gameViewport.Y + 20, width, 50);
            //   graphicsBtn.MouseUp += (sender, args) => GlobalVar.OptionsState = OptionsState.Graphics;

            controlsBtn.Text.Value = "Controls";
            controlsBtn.Bounds = new Rectangle(gameViewport.Left + 25, gameViewport.Y + 90, width, 50);
            //   controlsBtn.MouseUp += (sender, args) => GlobalVar.OptionsState = OptionsState.Controls;

            soundBtn.Text.Value = "Sounds";
            soundBtn.Bounds = new Rectangle(gameViewport.Left + 25, gameViewport.Y + 160, width, 50);
            // soundBtn.MouseUp += (sender, args) => GlobalVar.OptionsState = OptionsState.Sounds;

            cameraBtn.Text.Value = "Camera";
            cameraBtn.Bounds = new Rectangle(gameViewport.Left + 25, gameViewport.Y + 230, width, 50);
            // cameraBtn.CheckedChanged += (sender, args) => GlobalVar.OptionsState = OptionsState.Camera;

            group1.AddToList(graphicsBtn);
            group1.AddToList(controlsBtn);
            group1.AddToList(soundBtn);
            group1.AddToList(cameraBtn);
            
            

            Controls.Add(group1);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (GlobalVar.OptionsState == OptionsState.Main)
                base.Draw(spriteBatch);
            else if (GlobalVar.OptionsState == OptionsState.Graphics)
            {
                graphicDesign.Draw(spriteBatch);
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (GlobalVar.OptionsState == OptionsState.Main)
                base.Update(gameTime);
            else if (GlobalVar.OptionsState == OptionsState.Graphics)
            {
                graphicDesign.Update(gameTime);
            }
        }

        public enum OptionsState
        {
            Main,
            Graphics,
            Controls,
            Sounds,
            Camera
        }
    }
}
