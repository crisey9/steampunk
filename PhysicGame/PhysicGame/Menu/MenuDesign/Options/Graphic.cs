﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Controls;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PhysicGame.Menu.MenuDesign.Options
{
    class Graphic : MenuCore
    {
        private Button graphicsBtn;
        private Button controlsBtn;
        private Button soundBtn;
        private Button cameraBtn;

        public Graphic(Game game, Menu.Options manager) : base(game)
        {
            graphicsBtn = new Button(game);
            controlsBtn = new Button(game);
            soundBtn = new Button(game);
            cameraBtn = new Button(game);
        }

        public void LoadContent()
        {
            graphicsBtn.LoadContent();
            controlsBtn.LoadContent();
            soundBtn.LoadContent();
            cameraBtn.LoadContent();

            base.LoadContent();
        }

        public void Initialize()
        {
            graphicsBtn.Initialize();
            controlsBtn.Initialize();
            soundBtn.Initialize();
            cameraBtn.Initialize();

            base.Initialize();
        }

        public void SetDesign()
        {
            Controls = new List<Control>();
            int width = 250;

            graphicsBtn.Text.Value = "1";
            graphicsBtn.Bounds = new Rectangle(centerBtnPos.X - (width / 2), centerBtnPos.Y, width, 60);
            graphicsBtn.MouseUp += (sender, args) => GlobalVar.OptionsState = Menu.Options.OptionsState.Graphics;

            controlsBtn.Text.Value = "2";
            controlsBtn.Bounds = new Rectangle(centerBtnPos.X - (width / 2), centerBtnPos.Y + 80, width, 60);
            controlsBtn.MouseUp += (sender, args) => GlobalVar.OptionsState = Menu.Options.OptionsState.Graphics;

            soundBtn.Text.Value = "3";
            soundBtn.Bounds = new Rectangle(centerBtnPos.X - (width / 2), centerBtnPos.Y + 160, width, 60);
            soundBtn.MouseUp += (sender, args) => GlobalVar.OptionsState = Menu.Options.OptionsState.Graphics;

            cameraBtn.Text.Value = "4";
            cameraBtn.Bounds = new Rectangle(centerBtnPos.X - (width / 2), centerBtnPos.Y + 240, width, 60);
            cameraBtn.MouseUp += (sender, args) => GlobalVar.OptionsState = Menu.Options.OptionsState.Graphics;

            Controls.Add(soundBtn);
            Controls.Add(controlsBtn);
            Controls.Add(graphicsBtn);
            Controls.Add(cameraBtn);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
