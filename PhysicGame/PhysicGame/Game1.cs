﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Graphic;
using GameLibrary.Map;
using GameLibrary.Map.RogueLike;
using GameLibrary.Mouse;
using GameLibrary.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Penumbra;
using PhysicGame.Objects;
using PhysicGame.Pathfinding;

namespace PhysicGame
{
    /// <summary>
    ///     This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private MenuManager menuManager;

        private AStar aStar;
        private Player _player;
        private World _world;
        private readonly ToolsInGame _tools;
        private readonly FramerateCounter fpsCounter;
        private Camera camera;
        private SpawnMonsters monsters;
        private CorporationGenerator generator;
        private readonly GraphicManager graphManager;
        private SpriteBatch spriteBatch;
        Light light = new PointLight
        {
            Scale = new Vector2(1000f), // Range of the light source (how far the light will travel)
            ShadowType = ShadowType.Solid // Will not lit hulls themselves
        };

        private PenumbraComponent penumbra;


        public Game1()
        {
            Content.RootDirectory = "Content";
            var graphics = new GraphicsDeviceManager(this);
            graphManager = new GraphicManager(this, graphics);
            graphManager.DebugMode = true;
            //graphManager.PreferMultiSampling = true;
            graphManager.VSync = false;
            graphManager.IsFullScreen = false;
            graphManager.WindowSize = new Point(1280, 720);
            IsMouseVisible = true;
            fpsCounter = new FramerateCounter(this);
            _tools = new ToolsInGame(this);
            menuManager = new MenuManager(this);
            aStar = new AStar();
            penumbra = new PenumbraComponent(this);
            Components.Add(penumbra);
            generator = new CorporationGenerator(this, penumbra);
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
           // camera = new Camera(GraphicsDevice);
            base.Initialize();


            _world = new World(this);

            _world.Map.TileManager = generator.CreateMap();
            _world.Map.CollisionMap = generator.CollisionMap;
            _world.Initialize();



            _player = new Player(this, _world.Map.CollisionMap, camera, penumbra);
            _player.Position = generator.GetSpawnPos();
            _player.Initialize();

            camera.Debug.Grid.AddLines(32, Color.White, 2);
            camera.Debug.IsVisible = true;

            Song song = Content.Load<Song>("Songs/menu");

            MediaPlayer.Play(song);
            MediaPlayer.IsRepeating = true;

            menuManager.Initialize();
            GlobalVar.GameState = MenuManager.GameState.MainMenu;
            camera.Scale = 1.6f;// 1.7f;

            penumbra.Lights.Add(light);
            penumbra.Initialize();
            light.Color = Color.WhiteSmoke;
            Color color = new Color
            {
                R = 12,
                G = 15,
                B = 13,
                A = 255
            };
            penumbra.AmbientColor = color;

            monsters = new SpawnMonsters(this, generator, _world.Map, camera, _player, 1,3);
            monsters.LoadContent();
          
            _tools.Initialize(_player, _world, generator, penumbra);
        }

        protected override void LoadContent()
        {

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            camera.Debug.LoadContent(GraphicsDevice);
            _tools.LoadContent();
            menuManager.LoadContent();

            // TODO: use this.Content to load your game content here
        }

        protected override void UnloadContent()
        {
            Content.Unload();
            // TODO: Unload any non ContentManager content here
        }

        private void UpdateInGameLogic(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            KeyboardState state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.OemMinus))
                camera.Zoom(TimeSpan.FromSeconds(0.01), camera.Scale - 0.03f);
            if (state.IsKeyDown(Keys.OemPlus))
                camera.Zoom(TimeSpan.FromSeconds(0.01), to: camera.Scale + 0.03f);

            if (state.IsKeyDown(Keys.O))
            {
                aStar = new AStar();
                aStar.Map = _world.Map;
                aStar.StartingNode = new Node(new Point((int)camera.Position.X / generator.CellSize, (int)camera.Position.Y / generator.CellSize));
            }
            if (state.IsKeyDown(Keys.P))
            {
                aStar.DestinationNode = new Node(new Point((int)camera.Position.X / generator.CellSize, (int)camera.Position.Y / generator.CellSize));
                aStar.FindPath();

                foreach (Vector2 vector2 in aStar.GetFinalPath())
                {
                    Console.WriteLine(vector2);
                }

                aStar.SetDrawAstar();
            }


            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            light.Position = _player.GetBounds.Center.ToVector2();       

            _player.Update(gameTime);
            monsters.Update(gameTime);
            fpsCounter.Update(gameTime);
            _tools.Update(gameTime);
            camera.Position = _player.GetBounds.Center.ToVector2();
            camera.Update(gameTime);
            penumbra.Transform = camera.CreateTransform();

        }

        private void DrawInGame(GameTime gameTime)
        {
            _world.Draw(spriteBatch, camera, _player.GetBounds, _tools.coll);
            monsters.Draw(spriteBatch);
            _player.Draw(spriteBatch);
        }

        protected override void Update(GameTime gameTime)
        {
            if (IsActive)
                if (GlobalVar.GameState == MenuManager.GameState.InGame)
                {
                    UpdateInGameLogic(gameTime);
                }
                else
                {
                    menuManager.Update(gameTime);
                }
        }

        protected override void Draw(GameTime gameTime)
        {
            penumbra.BeginDraw();

            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            if (GlobalVar.GameState == MenuManager.GameState.InGame)
            {
                DrawInGame(gameTime);
                penumbra.Draw(gameTime);
            }
            else
            {
                spriteBatch.Begin();
                base.Draw(gameTime);
                spriteBatch.End();

                menuManager.Draw(spriteBatch);
            }

            if (GlobalVar.GameState == MenuManager.GameState.InGame)
            {
                _tools.Draw(spriteBatch, camera, gameTime);
                _player.DrawConditions(spriteBatch);
            }

        }
    }
}