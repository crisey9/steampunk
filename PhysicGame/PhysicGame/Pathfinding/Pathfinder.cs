﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GameLibrary.Map;
using GameLibrary.Movement;
using Microsoft.Xna.Framework;
using PhysicGame.Objects;

namespace PhysicGame.Pathfinding
{
    public class Pathfinder
    {
        private AStar aStar;
        public List<Vector2> nodes;
        private int acctually = 0;

        public Pathfinder(Creature creature, Point destination, Map map)
        {
            aStar = new AStar();
            nodes = new List<Vector2>();

            aStar.Map = map;
            aStar.CreateNodeList();
            aStar.GenerateCollisionNodes();
            aStar.StartingNode = new Node(new Point((int)creature.Position.X / 48, (int)creature.Position.Y / 48));
            aStar.DestinationNode = new Node(destination);
            aStar.FindPath();
            aStar.SetDrawAstar();

            nodes = aStar.GetFinalPath();
            foreach (var vector2 in nodes)
            {
                Console.WriteLine(vector2);
            }
            ;
        }

        public void MoveTo(CannonMonster creature)
        {
            creature.TileChanged += (sender, args) => Creature_TileChanged(creature);

          
        }

        private void Creature_TileChanged(CannonMonster creature)
        {
          


        }
    }
}
