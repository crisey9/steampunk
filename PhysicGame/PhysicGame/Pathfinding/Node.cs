﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PhysicGame.Pathfinding
{
    public class Node
    {
        public int H_Value { get; set; }
        public int G_Value { get; set; }
        public int F_Value { get; set; }

        public Node Parent { get; set; }
        public bool Passable { get; set; } = true;

        public Point Position { get; set; }

        public Node(Point pos)
        {
            Position = pos;
        }
    }
}
