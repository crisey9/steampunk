﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Graphic;
using GameLibrary.Map;
using Microsoft.Xna.Framework;

namespace PhysicGame.Pathfinding
{
    public class AStar
    {
        public Map Map { get; set; }
        Node CurrentNode;
        public bool ReachedTarget;

        public List<Node> OpenList { get; set; }
        public List<Node> ClosedList { get; set; }
        public List<Node> FinalPath { get; set; }

        public Node StartingNode { get; set; }
        public Node DestinationNode { get; set; }

        private Node[,] Nodes;

        public void CreateNodeList()
        {
            Nodes = new Node[Map.TileManager.Rows.Count, Map.TileManager.Rows[0].Columns.Count];

            for (int i = 0; i < Map.TileManager.Rows.Count; i++)
            {
                for (int j = 0; j < Map.TileManager.Rows[i].Columns.Count; j++)
                {
                    Nodes[j, i] = new Node(new Point(j, i));
                }
            }
        }

        public void GenerateCollisionNodes()
        {
            for (int i = 0; i < Map.CollisionMap.CollisionTiles.Count; i++)
            {
                Nodes[Map.CollisionMap.CollisionTiles[i].PosGet.X, Map.CollisionMap.CollisionTiles[i].PosGet.Y].Passable
                    = false;
            }
        }

        void FindNeighborsNodes()
        {
            Node NeighborNode;
            bool Add = false;

            OpenList.Remove(CurrentNode);

            if (!ClosedList.Contains(CurrentNode))
                ClosedList.Add(CurrentNode);


            //Check if we are in the bounds of the map.
            //Top Node
            if (CurrentNode.Position.Y - 1 >= 0)
            {
                NeighborNode = Nodes[(int)CurrentNode.Position.X, (int)CurrentNode.Position.Y - 1];

                if (!OpenList.Contains(NeighborNode))
                {
                    if (NeighborNode.Passable)
                    {
                        for (int i = 0; i < ClosedList.Count; i++)
                        {
                            if (NeighborNode.Position == ClosedList[i].Position)
                            {
                                Add = false;
                                break;
                            }
                            else
                                Add = true;
                        }
                    }

                    if (Add)
                    {
                        NeighborNode.Parent = CurrentNode;
                        OpenList.Add(NeighborNode);
                        Add = false;
                    }
                }
            }

            //Bottom Node
            if (CurrentNode.Position.Y + 1 < Nodes.GetLength(1))
            {
                NeighborNode = Nodes[(int)CurrentNode.Position.X, (int)CurrentNode.Position.Y + 1];

                if (!OpenList.Contains(NeighborNode))
                {
                    if (NeighborNode.Passable)
                    {
                        for (int i = 0; i < ClosedList.Count; i++)
                        {
                            if (NeighborNode.Position == ClosedList[i].Position)
                            {
                                Add = false;
                                break;
                            }
                            else
                                Add = true;
                        }
                    }

                    if (Add)
                    {
                        NeighborNode.Parent = CurrentNode;
                        OpenList.Add(NeighborNode);
                        Add = false;
                    }
                }
            }


            //Right Node
            if (CurrentNode.Position.X + 1 < Nodes.GetLength(0))
            {
                NeighborNode = Nodes[(int)CurrentNode.Position.X + 1, (int)CurrentNode.Position.Y];

                if (!OpenList.Contains(NeighborNode))
                {
                    if (NeighborNode.Passable)
                    {
                        for (int i = 0; i < ClosedList.Count; i++)
                        {
                            if (NeighborNode.Position == ClosedList[i].Position)
                            {
                                Add = false;
                                break;
                            }
                            else
                                Add = true;
                        }
                    }

                    if (Add)
                    {
                        NeighborNode.Parent = CurrentNode;
                        OpenList.Add(NeighborNode);
                        Add = false;
                    }
                }
            }

            //Left Node
            if (CurrentNode.Position.X - 1 >= 0)
            {
                NeighborNode = Nodes[(int)CurrentNode.Position.X - 1, (int)CurrentNode.Position.Y];

                if (!OpenList.Contains(NeighborNode))
                {
                    if (NeighborNode.Passable)
                    {
                        for (int i = 0; i < ClosedList.Count; i++)
                        {
                            if (NeighborNode.Position == ClosedList[i].Position)
                            {
                                Add = false;
                                break;
                            }
                            else
                                Add = true;
                        }
                    }

                    if (Add)
                    {
                        NeighborNode.Parent = CurrentNode;
                        OpenList.Add(NeighborNode);
                        Add = false;
                    }
                }
            }
        }

        void Calculate_H_Value()
        {
            //distance from the starting node to the ending node.
            Vector2 Distance = Vector2.Zero;

            for (int x = 0; x < Nodes.GetLength(0); x++)
            {
                for (int y = 0; y < Nodes.GetLength(1); y++)
                {
                    Vector2 CurrentNodePosition = new Vector2(x, y);

                    //Current Node at Right
                    if (CurrentNodePosition.X <= DestinationNode.Position.X)
                        Distance.X = DestinationNode.Position.X - CurrentNodePosition.X;

                    //Current Node at Left
                    if (CurrentNodePosition.X >= DestinationNode.Position.X)
                        Distance.X = CurrentNodePosition.X - DestinationNode.Position.X;


                    //Current Node at Up
                    if (CurrentNodePosition.Y <= DestinationNode.Position.Y)
                        Distance.Y = DestinationNode.Position.Y - CurrentNodePosition.Y;

                    //Current Node at Down
                    if (CurrentNodePosition.Y >= DestinationNode.Position.Y)
                        Distance.Y = CurrentNodePosition.Y - DestinationNode.Position.Y;

                    Nodes[x, y].H_Value = (int)(Distance.X + Distance.Y);
                }
            }
        }

        void Calculate_G_Value()
        {
            for (int i = 0; i < OpenList.Count; i++)
            {
                OpenList[i].G_Value = OpenList[i].Parent.G_Value + 10;
            }
        }

        void Calculate_F_Value()
        {
            for (int i = 0; i < OpenList.Count; i++)
                OpenList[i].F_Value = OpenList[i].G_Value + OpenList[i].H_Value;
        }

        Node GetMin_F_Vaule()
        {
            List<int> F_Values_List = new List<int>();
            int Lowest_F_Vaule = 0;


            for (int i = 0; i < OpenList.Count; i++)
                F_Values_List.Add(OpenList[i].F_Value);

            if (F_Values_List.Count > 0)
            {
                Lowest_F_Vaule = F_Values_List.Min();

                for (int i = 0; i < OpenList.Count; i++)
                {
                    if (OpenList[i].F_Value == Lowest_F_Vaule)
                        return OpenList[i];
                }
            }

            return CurrentNode;
        }

        void CalculateFinalPath()
        {
            List<Node> FinalPathTemp = new List<Node>();

            FinalPathTemp.Add(CurrentNode);
            FinalPathTemp.Add(CurrentNode.Parent);

            for (int i = 1; ; i++)
            {
                if (FinalPathTemp[i].Parent != null)
                    FinalPathTemp.Add(FinalPathTemp[i].Parent);
                else
                    break;
            }

            // reverse the list elemnts order from last to to begining.
            for (int i = FinalPathTemp.Count - 1; i >= 0; i--)
                FinalPath.Add(FinalPathTemp[i]);
        }

        /// <summary>
        /// Returns a set of Vector2 coordinate of the shortest path from the starting point to the goal or ending point (if found) in map coordinates;
        /// </summary>
        /// <returns></returns>
        public List<Vector2> GetFinalPath()
        {
            List<Vector2> FinalPathVec2 = new List<Vector2>();

            if (FinalPath != null)
                for (int i = 1; i < FinalPath.Count; i++)
                    FinalPathVec2.Add(FinalPath[i].Position.ToVector2());

            return FinalPathVec2;
        }

        public void SetDrawAstar()
        {
            foreach (var vector2 in GetFinalPath())
            {
                for (int i = 0; i < Map.TileManager.Rows.Count; i++)
                {
                    for (int j = 0; j < Map.TileManager.Rows[i].Columns.Count; j++)
                    {
                        if (Map.TileManager.Rows[i].Columns[j].Tile.PosGet.ToVector2().Equals(vector2))
                        {
                            Map.TileManager.Rows[i].Columns[j].Tile.Astar = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="StartingNode">The starting position of the search path in map coordinate</param>
        /// <param name="EndingNode">The Goal or the ending position of the search path in map coordinate</param>
        public void FindPath()
        {
            CreateNodeList();
            GenerateCollisionNodes();

            CurrentNode = this.StartingNode;

            OpenList = new List<Node>();
            ClosedList = new List<Node>();
            FinalPath = new List<Node>();

            Calculate_H_Value();

            while (true)
            {
                CurrentNode = GetMin_F_Vaule();

                FindNeighborsNodes();
                Calculate_G_Value();
                Calculate_F_Value();

                for (int i = 0; i < OpenList.Count; i++)
                {
                    if (OpenList[i].Position == DestinationNode.Position)
                    {
                        CurrentNode = OpenList[i];
                        CalculateFinalPath();
                        ReachedTarget = true;
                        break;
                    }
                }

                if (ReachedTarget)
                    break;

                if (OpenList.Count == 0)
                {
                    ReachedTarget = false;
                    break;
                }
            }
        }
    }
}
