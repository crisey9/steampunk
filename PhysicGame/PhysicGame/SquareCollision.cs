﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Graphic;
using GameLibrary.Map;
using GameLibrary.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PhysicGame.Objects;

namespace PhysicGame
{
    public class SquareCollision : DrawableGameComponent
    {
        public Collision CollisionMap { get; set; }
        private readonly Camera _camera;
        private Rectangle _bounds;
        public Rectangle Bounds
        {
            get => new Rectangle(_bounds.X + SmallerCollision.X / 2,
                _bounds.Y + SmallerCollision.Y / 2,
                _bounds.Width - SmallerCollision.X,
                _bounds.Height - SmallerCollision.Y);
            set => _bounds = value;
        }
        public Point SmallerCollision { get; set; } = new Point(12, 7);
        public bool Collision { get; set; } = true;
        public bool CollisionDraw { get; set; } = true;

        private Texture2D _texture;
        public Texture2D Texture
        {
            get => _texture ?? Game.Content.Load<Texture2D>("Textures/UI/box");
            set => _texture = value;
        }

        private Player _player;

        public SquareCollision(Game game, Collision colMap, Camera camera, Player player) : base(game)
        {
            CollisionMap = colMap;
            _camera = camera;
            _player = player;
        }

        public SquareCollision(Game game, Player player, Camera camera) : base(game)
        {
            _camera = camera;
            _player = player;
        }

        public bool IsWalkableCollision(Vector2 velocity, bool godmode)
        {
            var tmpb = Bounds;
            var tmp = Bounds;
            tmp.X += (int)velocity.X;
            tmp.Y += (int)velocity.Y;

            Bounds = tmp;
            if (!GetRectangleCollision() || godmode)
            {
                Bounds = tmpb;
                return true;
            }
            Bounds = tmpb;
            return false;
        }

        public bool IsTriggered()
        {
            return Bounds.Intersects(new Rectangle(_player.GetBounds.Center, new Point(_player.Width/2, _player.Height/2)));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!CollisionDraw) return;
            spriteBatch.Begin(_camera);
            spriteBatch.Draw(Game.Content.Load<Texture2D>("Textures/frame"), Bounds, Color.Pink);
            spriteBatch.End();
        }

        private bool GetRectangleCollision()
        {
            return CollisionMap.CollisionTiles.Any(tile => tile.Bounds.Intersects(Bounds));
        }
    }
}
