﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Graphic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Penumbra;

namespace GameLibrary.Map.RogueLike
{
    public class CorporationGenerator : DrawableGameComponent
    {
        private PenumbraComponent _penumbra;
        public TileManager TileManager { get; private set; }
        public Collision CollisionMap { get; private set; }

        public Rectangle SpawnBounds { get; set; } = new Rectangle(10, 10, 10, 10);

        public int CellSize { get; set; } = 32;

        public List<Room> Rooms { get; set; }
        public List<Corridor> Corridors { get; set; }


        public int MapHeight { get; set; } = 80;
        public int MapWidth { get; set; } = 80;

        public int MaxRooms { get; set; } = 10;
        public int MinRooms { get; set; } = 5;

        public int MaxCorridors { get; set; } = 5;
        public int MinCorridors { get; set; } = 3;

        private Generator.RoomTextures roomTextures;

        public CorporationGenerator(Game game, PenumbraComponent penumbra) : base(game)
        {
            Reset();

            this._penumbra = penumbra;
        }

        public void Reset()
        {
            CollisionMap = new Collision();
            Rooms = new List<Room>();
            Corridors = new List<Corridor>();
        }

        public TileManager CreateMap()
        {
            TileManager = new TileManager(Game);
            _penumbra.Hulls.Clear();
            roomTextures = new Generator.RoomTextures(Game);

            for (int i = 0; i < MapHeight; i++)
            {
                TileManager.Rows.Add(new TileManager.Row());
                TileManager.Rows[i].Columns = new Collection<TileManager.Column>();
                for (int j = 0; j < MapWidth; j++)
                {
                    var tile = new TileManager.Tile(Game);
                    TileManager.Rows[i].Columns.Add(new TileManager.Column());
                    TileManager.Rows[i].Columns[j].Tile = new TileManager.Tile(Game);

                    tile.Texture = roomTextures.BlackHole;
                    tile.CollisionType = CollisionType.Block;
                    CollisionMap.CollisionTiles.Add(tile);
                    tile.Bounds = new Rectangle(j * CellSize, i * CellSize, CellSize, CellSize);

                    TileManager.Rows[i].Columns[j].Tile = tile;
                }
            }

            Random rnd = new Random();

            Corridors.Add(new Corridor(Game, SpawnBounds));

            var corridorsCount = rnd.Next(MinCorridors, MaxCorridors);
            int correctCorridors = 0;

            while (correctCorridors < corridorsCount)
            {
                var height = rnd.Next(Corridor.MinHeight, Corridor.MaxHeight);
                var width = rnd.Next(Corridor.MinWidth, Corridor.MaxWidth);

                var posX = rnd.Next(0, MapWidth - width);
                var posY = rnd.Next(0, MapHeight - height);


                var bounds = new Rectangle(posX, posY, width, height);
                bool corridorIntersects = false;
                foreach (var corridor in Corridors)
                {
                    if (bounds.Intersects(corridor.Bounds))
                    {
                        corridorIntersects = true;
                        break;
                    }
                }
                if (!corridorIntersects)
                {
                    Corridors.Add(new Corridor(Game, bounds));
                    correctCorridors++;
                }
            }




            foreach (var corridor in Corridors)
            {
                Console.WriteLine(corridor.Bounds.ToString());
                CreateRoom(corridor.Bounds);
            }

            for (int i = 1; i < Corridors.Count; i++)
            {
                var currentCorridorCenter = new Point(Corridors[i].Bounds.Center.X, Corridors[i].Bounds.Center.Y);
                var previusCorridorCenter = new Point(Corridors[i - 1].Bounds.Center.X, Corridors[i - 1].Bounds.Center.Y);


                CreateVerticalTunnel(previusCorridorCenter, currentCorridorCenter);
                CreateHorizontalTunnel(currentCorridorCenter, previusCorridorCenter);


            }

            return TileManager;
        }

        private void CreateRoom(Rectangle rect)
        {
            for (int i = rect.Left; i <= rect.Right; i++)
            {
                for (int j = rect.Top; j <= rect.Bottom; j++)
                {
                    if (rect.Top <= j)
                    {
                        bool canDefault = true;

                        if (j == rect.Top) //top
                        {
                            CreateCell(i, j, roomTextures.TopWall, CollisionType.Block, WallDirection.Top);
                            canDefault = false;
                        }
                        if (j == rect.Bottom) //bottom
                        {
                            CreateCell(i, j, roomTextures.BottomWall, CollisionType.Block, WallDirection.Bottom);
                            canDefault = false;
                        }

                        if (i == rect.Right) //right
                        {
                            CreateCell(i, j, roomTextures.RightWall, CollisionType.Block, WallDirection.Right);
                            canDefault = false;
                        }
                        if (i == rect.Left) //left
                        {
                            CreateCell(i, j, roomTextures.LeftWall, CollisionType.Block, WallDirection.Left);
                            canDefault = false;
                        }



                        if (i == rect.Left && j == rect.Top)
                        {
                            CreateCell(i, j, roomTextures.LeftTopWall, CollisionType.Block, WallDirection.LeftTop);
                            canDefault = false;
                        }
                        if (i == rect.Left && j == rect.Bottom)
                        {
                            CreateCell(i, j, roomTextures.BottomLeftWall, CollisionType.Block, WallDirection.BottomLeft);
                            canDefault = false;
                        }
                        if (i == rect.Right && j == rect.Bottom)
                        {
                            CreateCell(i, j, roomTextures.RightBottomWall, CollisionType.Block, WallDirection.RightBottom);
                            canDefault = false;
                        }
                        if (i == rect.Right && j == rect.Top)
                        {
                            CreateCell(i, j, roomTextures.TopRightWall, CollisionType.Block, WallDirection.TopRight);
                            canDefault = false;
                        }

                        if (j == rect.Top + 1 && i > rect.Left && i < rect.Right)
                        {
                            CreateCell(i, j, roomTextures.WallBiome.UpTopWall, CollisionType.Block, WallDirection.Null, true);
                            canDefault = false;
                        }
                        if (j == rect.Top + 2 && i > rect.Left && i < rect.Right)
                        {
                            CreateCell(i, j, roomTextures.WallBiome.BottomTopWall, CollisionType.Block, WallDirection.Null, true);
                            canDefault = false;
                        }


                        if (canDefault)
                            CreateCell(i, j, roomTextures.Default);
                    }
                }
            }
        }

        public void CreateCell(int x, int y, Texture2D texture, CollisionType collision = CollisionType.None, WallDirection direction = WallDirection.Null, bool lightAnyway = false)
        {
            if (collision == CollisionType.None)
                CollisionMap.CollisionTiles.Remove(TileManager.Rows[y].Columns[x].Tile);
            TileManager.Rows[y].Columns[x].Tile.Texture = texture;
            TileManager.Rows[y].Columns[x].Tile.CollisionType = collision;
            var t = _penumbra.Hulls.FirstOrDefault(tmp => tmp.Position == new Vector2(x * CellSize, y * CellSize));
            _penumbra.Hulls.Remove(t);
            var hull = new Hull();

            if (direction == WallDirection.Top)
            {
                hull = new Hull(new Vector2(0, CellSize - Generator.RoomTextures.WallMargin.Top -1),
                    new Vector2(CellSize, CellSize - Generator.RoomTextures.WallMargin.Top - 1),
                    new Vector2(CellSize, CellSize - Generator.RoomTextures.WallMargin.Top),
                    new Vector2(0f, CellSize - Generator.RoomTextures.WallMargin.Top));
            }
            else if (direction == WallDirection.Right)
            {
                hull = new Hull(new Vector2(Generator.RoomTextures.WallMargin.Right, 0),
                    new Vector2(Generator.RoomTextures.WallMargin.Right + 3, 0),
                    new Vector2(Generator.RoomTextures.WallMargin.Right + 3, CellSize),
                    new Vector2(Generator.RoomTextures.WallMargin.Right, CellSize));
            }
            else if (direction == WallDirection.Bottom)
            {
                hull = new Hull(new Vector2(0, Generator.RoomTextures.WallMargin.Bottom),
                    new Vector2(CellSize, Generator.RoomTextures.WallMargin.Bottom),
                    new Vector2(CellSize, CellSize + 1),
                    new Vector2(0, CellSize + 1));
            }
            else if (direction == WallDirection.Left)
            {
                hull = new Hull(new Vector2(CellSize-Generator.RoomTextures.WallMargin.Left, 0),
                    new Vector2(CellSize - Generator.RoomTextures.WallMargin.Left -2, 0f),
                    new Vector2(CellSize - Generator.RoomTextures.WallMargin.Left -2, CellSize),
                    new Vector2(CellSize - Generator.RoomTextures.WallMargin.Left, CellSize));
            }

            else if (direction == WallDirection.TopRight)
            {
                hull = new Hull(new Vector2(0),
                    new Vector2(CellSize, 0f),
                    new Vector2(CellSize, CellSize),
                    new Vector2(Generator.RoomTextures.WallMargin.Right, CellSize),
                    new Vector2(Generator.RoomTextures.WallMargin.Right, CellSize - Generator.RoomTextures.WallMargin.Top),
                    new Vector2(0f, CellSize - Generator.RoomTextures.WallMargin.Top));
            }
            else if (direction == WallDirection.RightBottom)
            {

                hull = new Hull(new Vector2(Generator.RoomTextures.WallMargin.Right, 0),
                                new Vector2(CellSize, 0f),
                                new Vector2(CellSize, CellSize),
                                new Vector2(0, CellSize + 1),
                                new Vector2(0, CellSize),
                                new Vector2(Generator.RoomTextures.WallMargin.Right, CellSize));
            }
            else if (direction == WallDirection.BottomLeft)
            {
                hull = new Hull(new Vector2(0),
                    new Vector2(CellSize - Generator.RoomTextures.WallMargin.Left, 0f),
                    new Vector2(CellSize - Generator.RoomTextures.WallMargin.Left, CellSize),
                    new Vector2(CellSize, CellSize),
                    new Vector2(CellSize, CellSize + 1),
                    new Vector2(0f, CellSize + 1));
            }
            else if (direction == WallDirection.LeftTop)
            {
                hull = new Hull(new Vector2(0),
                    new Vector2(CellSize, 0),
                    new Vector2(CellSize, CellSize - Generator.RoomTextures.WallMargin.Top),
                    new Vector2(CellSize - Generator.RoomTextures.WallMargin.Left, CellSize - Generator.RoomTextures.WallMargin.Top),
                    new Vector2(CellSize - Generator.RoomTextures.WallMargin.Left, CellSize),
                    new Vector2(0f, CellSize));
            }


             else if (collision == CollisionType.Block && !lightAnyway)
             {
                 hull = new Hull(new Vector2(0), new Vector2(0f, 1.0f), new Vector2(1.0f), new Vector2(1.0f, 0))
                 {
                     Scale = new Vector2(CellSize)
                 };
             }

            hull.Position = new Vector2(x * CellSize, y * CellSize);

            if (hull != new Hull())
                _penumbra.Hulls.Add(hull);
        }

        private void CreateHorizontalTunnel(Point start, Point end)
        {
            for (int i = Math.Min(start.X, end.X); i <= Math.Max(start.X, end.X); i++)
            {
                CreateCell(i, end.Y, roomTextures.Default);
                /*                CreateCell(i, end.Y - 1, roomTextures.WallBiome.BottomTopWall);
                                CreateCell(i, end.Y - 2, roomTextures.WallBiome.UpTopWall);
                                CreateCell(i, end.Y - 3, roomTextures.TopWall);
                                CreateCell(i, end.Y + 1, roomTextures.BottomWall);
                     CreateCell(i + 1, end.Y + 1, roomTextures.RightBottomWall);
                     CreateCell(i + 1, end.Y - 1, roomTextures.RightWall);
                     CreateCell(i + 1, end.Y - 2, roomTextures.RightWall);
                     CreateCell(i + 1, end.Y - 3, roomTextures.TopRightWall);*/



            }
        }

        private void CreateVerticalTunnel(Point start, Point end)
        {
            int j = Math.Max(start.Y, end.Y);
            for (int i = Math.Min(start.Y, end.Y); i <= j; i++)
            {
                //   CreateCell(end.X + 1, i, roomTextures.RightWall);
                CreateCell(end.X, i, roomTextures.Default);
                //   CreateCell(end.X - 1, i, roomTextures.LeftWall);
                /*   if (i == j)
                   {
                       CreateCell(end.X + 1, i + 1, roomTextures.RightBottomWall);

                   }*/
            }
        }

        public Position GetSpawnPos()
        {
            return new Position((SpawnBounds.X + (SpawnBounds.Width / 2)) * CellSize, (SpawnBounds.Y + (SpawnBounds.Height / 2)) * CellSize);
        }

        public class Corridor : BaseGrid
        {
            public Corridor(Game game, Rectangle rectangle) : base(game, rectangle)
            {

            }
        }

        public class Room : BaseGrid
        {
            public Room(Game game, Rectangle rectangle) : base(game, rectangle)
            {

            }
        }

        public class BaseGrid : GameComponent
        {
            public const int MaxWidth = 16;
            public const int MinWidth = 8;
            public const int MaxHeight = 16;
            public const int MinHeight = 8;

            public Rectangle Bounds { get; set; }

            public BaseGrid(Game game, Rectangle rectangle) : base(game)
            {
                Bounds = rectangle;
            }
        }

        public enum WallDirection : ushort
        {
            Top,
            TopRight,
            Right,
            RightBottom,
            Left,
            LeftTop,
            Bottom,
            BottomLeft,
            Null,
        }
    }
}
