﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using GameLibrary.Graphic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Map.RogueLike
{
    public class Generator : GameComponent
    {
        public Generator(Game game) : base(game)
        {
            
        }

        public class RoomTextures : GameComponent
        {
            public readonly WallBiome WallBiome;
            public readonly Texture2D TopWall;
            public readonly Texture2D RightWall;
            public readonly Texture2D BottomWall;
            public readonly Texture2D LeftWall;
            public readonly Texture2D TopRightWall;
            public readonly Texture2D RightBottomWall;
            public readonly Texture2D BottomLeftWall;
            public readonly Texture2D LeftTopWall;

            public readonly Texture2D Default;
            public readonly Texture2D BlackHole;

            public class WallMargin
            {
                public const int Left = 3;
                public const int Right = 3;
                public const int Top = 3;
                public const int Bottom = 32;

            }

            public RoomTextures(Game game) : base(game)
            {
                TilesetManager tileset = new TilesetManager(game)
                {
                    Texture = Game.Content.Load<Texture2D>("Textures/Room/Wall"),
                    SizeEachTile = new Point(32, 32),
                };
                Default = Game.Content.Load<Texture2D>("Textures/Floor");
                WallBiome = new WallBiome(game);
                TopWall = tileset.GetTexture(1, 0);
                RightWall = tileset.GetTexture(2, 1);
                BottomWall = tileset.GetTexture(1, 2);
                LeftWall = tileset.GetTexture(0, 1);
                BlackHole = tileset.GetTexture(1, 1);
                TopRightWall = tileset.GetTexture(2, 0);
                RightBottomWall = tileset.GetTexture(2, 2);
                BottomLeftWall = tileset.GetTexture(0, 2);
                LeftTopWall = tileset.GetTexture(0, 0);

            }
        }

        public class WallBiome : GameComponent
        {
            public readonly Texture2D UpTopWall;
            public readonly Texture2D BottomTopWall;

            public WallBiome(Game game) : base(game)
            {
                TilesetManager tileset = new TilesetManager(game)
                {
                    Texture = Game.Content.Load<Texture2D>("Textures/Room/TopWall/TopWall"),
                    SizeEachTile = new Point(32, 32),
                };

                UpTopWall = tileset.GetTexture(0, 0);
                BottomTopWall = tileset.GetTexture(0, 1);
            }
        }
    }
}
