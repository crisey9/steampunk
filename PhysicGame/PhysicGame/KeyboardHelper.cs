﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PhysicGame
{
    public class KeyboardHelper : GameComponent
    {
        public event EventHandler PressedCheat;
        public event EventHandler PressedCollision;
        public event EventHandler PressedActionMode;

        private KeyboardState oldState;
        private KeyboardState newState;

        public KeyboardHelper(Game game) : base(game)
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            newState = Keyboard.GetState();

            if (HandleInput(Keys.F1))
            {
                OnPressedCheat(EventArgs.Empty);
            }
            if (HandleInput(Keys.U))
            {
                OnPressedCollision(EventArgs.Empty);
            }
            if (HandleInput(Keys.D1))
            {
                OnPressedActionMode(EventArgs.Empty);
            }

            oldState = newState;
        }

        private bool HandleInput(Keys key)
        {
            return (oldState.IsKeyUp(key) && newState.IsKeyDown(key));
        }

        #region Events
        protected virtual void OnPressedCheat(EventArgs e)
        {
            EventHandler eh = PressedCheat;
            eh?.Invoke(this, e);
        }
        protected virtual void OnPressedCollision(EventArgs e)
        {
            EventHandler eh = PressedCollision;
            eh?.Invoke(this, e);
        }
        protected virtual void OnPressedActionMode(EventArgs e)
        {
            EventHandler eh = PressedActionMode;
            eh?.Invoke(this, e);
        }
        #endregion Events
    }
}
