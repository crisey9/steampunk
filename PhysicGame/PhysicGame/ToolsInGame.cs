﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Graphic;
using GameLibrary.Map;
using GameLibrary.Map.RogueLike;
using GameLibrary.Mouse;
using GameLibrary.UI.Controls;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Penumbra;
using PhysicGame.Objects;

namespace PhysicGame
{
    class ToolsInGame : DrawableGameComponent
    {
        private PenumbraComponent _penumbra;

        public int Margin { get; set; } = 10;

        private KeyboardHelper keyboardHelper;
        private MouseInput1 mouseInput;

        private Checkbox _godModeCheckBox;
        public Checkbox _shadersStateCheckBox;

        private Textbox _speedTxtBox;
        private Button _generateMapBtn;

        public bool coll;

        public bool CheatMode { get; set; } = false;

        public ToolsInGame(Game game) : base(game)
        {
            mouseInput = new MouseInput1(game);
            keyboardHelper = new KeyboardHelper(game);

            _godModeCheckBox = new Checkbox(game);
            _shadersStateCheckBox = new Checkbox(game);
            _speedTxtBox = new Textbox(game);
            _generateMapBtn = new Button(game);
        }

        public void LoadContent()
        {
            base.LoadContent();
            _godModeCheckBox.LoadContent();
            _shadersStateCheckBox.LoadContent();
            _speedTxtBox.LoadContent();
            _generateMapBtn.LoadContent();
        }

        public void Initialize(Player player, World world, CorporationGenerator generator, PenumbraComponent penumbra)
        {
            base.Initialize();
            this._penumbra = penumbra;
            var color = penumbra.AmbientColor;

            keyboardHelper.PressedCheat += KeyboardHelper_PressedCheat;
            keyboardHelper.PressedCollision += KeyboardHelper_PressedCollision;
            keyboardHelper.PressedActionMode += (sender, args) => KeyboardHelper_PressedActionMode(player);

            Rectangle rect = Game.GraphicsDevice.Viewport.Bounds;
            int x = rect.Width - 275;
            int y = 20;

            _godModeCheckBox.Text.Value = "GOD MODE";
            _godModeCheckBox.Bounds = new Rectangle(x, y, 32, 32);
            _godModeCheckBox.CheckedChanged += (sender, args) =>
            {
                player.GodMode = _godModeCheckBox.IsChecked;
            };

            _shadersStateCheckBox.Text.Value = "SHADERS";
            _shadersStateCheckBox.IsChecked = true;
            _shadersStateCheckBox.CheckedChanged += (senders, args) =>
            {
                if (!_shadersStateCheckBox.IsChecked)
                    _penumbra.AmbientColor = Color.White;
                else
                    _penumbra.AmbientColor = color;
            };

            _generateMapBtn.Click += (sender, args) => Btn_Click(world, player, generator);
            _generateMapBtn.Bounds = new Rectangle(20, 300, 0, 0);
            _generateMapBtn.Text.Value = "Generate new map";
            _generateMapBtn.AutoPosition = true;

            _generateMapBtn.Initialize();

            _speedTxtBox.Submit += (sender, args) =>
            {
                try
                {
                    player.Velocity = Convert.ToInt32(_speedTxtBox.Text.Value);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    _speedTxtBox.Text.Value = player.Velocity.ToString();
                }
            };


            _shadersStateCheckBox.Bounds = PositionUnderControl(_generateMapBtn, 32, 32);
            _speedTxtBox.Bounds = PositionUnderControl(_godModeCheckBox, 250, 40);
        }

        private void KeyboardHelper_PressedActionMode(Player player)
        {
            if (player.ActionMode != Player.Action.Sword && player.ActionMode != Player.Action.SwordAttack)
                player.ActionMode = Player.Action.Sword;
            else if (player.ActionMode == Player.Action.Sword)
                player.ActionMode = Player.Action.None;
        }

        private void KeyboardHelper_PressedCollision(object sender, EventArgs e)
        {
            coll = !coll;
        }

        private void Btn_Click(World world, Player player, CorporationGenerator generator)
        {
            generator.Reset();
            Stopwatch stop = new Stopwatch();
            stop.Start();
            world.Map.TileManager = generator.CreateMap();
            world.Map.CollisionMap = generator.CollisionMap;
            player.CollisionMap = world.Map.CollisionMap;
            world.Initialize();
            stop.Stop();

            Console.WriteLine($"Map created in: {stop.Elapsed.TotalMilliseconds} ms");
            stop.Reset();
        }

        private void KeyboardHelper_PressedCheat(object sender, EventArgs e)
        {
            CheatMode = !CheatMode;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            keyboardHelper.Update(gameTime);

            if (CheatMode)
            {
                _godModeCheckBox.Update(gameTime);
                _shadersStateCheckBox.Update(gameTime);
                _speedTxtBox.Update(gameTime);
                mouseInput.Update(gameTime);
                _generateMapBtn.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Camera camera, GameTime gameTime)
        {
            if (CheatMode)
            {
                camera.Debug.Draw(gameTime, spriteBatch, new Vector2(0, 0));
                _godModeCheckBox.Draw(spriteBatch);
                _shadersStateCheckBox.Draw(spriteBatch);
                _speedTxtBox.Draw(spriteBatch);
                mouseInput.Draw(spriteBatch, camera.Position);
                _generateMapBtn.Draw(spriteBatch);
            }
        }

        private Vector2 PositionUnderControl(Control control)
        {
            return new Vector2(control.Bounds.X, control.Bounds.Y + control.Bounds.Height + Margin);
        }

        private Rectangle PositionUnderControl(Control control, int width, int height)
        {
            return new Rectangle(control.Bounds.X, control.Bounds.Y + control.Bounds.Height + Margin, width, height);
        }
    }
}
