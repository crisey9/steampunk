﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Movement;
using GameLibrary.Graphic;
using GameLibrary.Graphic.Particles;
using GameLibrary.Map;
using GameLibrary.Mouse;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Penumbra;
using PhysicGame.Conditions;

namespace PhysicGame.Objects
{
    public class Player : Creature
    {
        private bool lastSwordAttack;
        private ParticleEngine particleEngine;

        public const int ConstSpeed = 205;
        public const int DiagonalSpeed = 145;

        private MouseInput1 mouse;
        private float angle;

        public Texture2D PlayerNoneActionTexture { get; set; }
        public Texture2D PlayerSwordTexture { get; set; }
        public Texture2D PlayerSwordAttackTexture { get; set; }

        private bool newParticles;

        public enum Action
        {
            None,
            Sword,
            SwordAttack,
        }

        private Spotlight spotlight;

        private PenumbraComponent penumbra;
        private Animator _anim;
        private readonly PlayerWorker _playerControll;
        public MovementState state { get; set; }

        public Condition Health;
        public Condition Mana;

        private Action _actionMode;
        public Action ActionMode
        {
            get { return _actionMode; }
            set
            {
                _actionMode = value;
                if (value == Action.Sword)
                    Texture = PlayerSwordTexture;
                else if (value == Action.None)
                {
                    Texture = PlayerNoneActionTexture;
                }
                else if (value == Action.SwordAttack)
                {
                    Texture = PlayerSwordAttackTexture;
                }

                _anim = new Animator(Game, Texture)
                {
                    Position = Position.ToVector2(),
                    HeightSingleSprite = Height,
                    WidthSingleSprite = Width,
                };
                _anim.AnimationDone += _anim_AnimationDone;
            }
        }


        private Collision _collisionMap;

        public Collision CollisionMap
        {
            get => _collisionMap;
            set
            {
                _collisionMap = value;
                collision = new SquareCollision(Game, value, camera, this);
            }
        }

        public SquareCollision collision;

        public bool GodMode { get; set; } = false;

        public Player(Game game, Collision collisionMap, Camera camera, PenumbraComponent penumbra) : base(game, camera)
        {
            Height = 48;
            Width = 48;
            this.camera = camera;
            _playerControll = new PlayerWorker(game);
            Velocity = 205;
            CollisionMap = collisionMap;
            Health = new Condition(game);
            Mana = new Condition(game);
            collision = new SquareCollision(game, collisionMap, camera, this);
            collision.CollisionDraw = false;
            mouse = new MouseInput1(game);
            mouse.MouseLeftClick += MouseOnMouseLeftClick;
            this.penumbra = penumbra;
        }

        private void _anim_AnimationDone(object sender, EventArgs e)
        {
            if (ActionMode == Action.SwordAttack)
            {
                if (penumbra.Lights[1] != null)
                {
                    penumbra.Lights.RemoveAt(1);
                }
                ActionMode = Action.Sword;
                lastSwordAttack = !lastSwordAttack;
            }
        }

        private void MouseOnMouseLeftClick(object sender, EventArgs eventArgs)
        {
            if (ActionMode == Action.Sword)
            {
                ActionMode = Action.SwordAttack;
                _anim.AnimationTime = 65;
                Random rnd = new Random();
                Color color = new Color
                {
                    R = (byte)rnd.Next(135, 160),
                    G = (byte)rnd.Next(245, 255),
                    B = (byte)rnd.Next(180, 220),
                    A = (byte)rnd.Next(200, 245)
                };
                spotlight = new Spotlight
                {
                    Scale = new Vector2(110),
                    ShadowType = ShadowType.Solid,
                    ConeDecay = (float)((float)rnd.Next(33, 180) / 100),
                    Position = this.GetBounds.Center.ToVector2() + new Vector2(rnd.Next(-4, 4), rnd.Next(-4, 4)),
                    Rotation = DegreeToRadian(angle - 22.5f),
                    Color = color
                };

                penumbra.Lights.Add(spotlight);
            }
        }

        public override void Initialize()
        {
            base.Initialize();
            _anim.Initialize();
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(Game.Content.Load<Texture2D>("Textures/ThePixel"));
            particleEngine = new ParticleEngine(textures, Game, camera);
            particleEngine.WidthRespawn = 10;
            particleEngine.HeightRespawn = 20;
            particleEngine.Util = new ParticleEngineUtil(100, 200, 1000, 1, 2, -100, 100, -1, 1, -1, 1, -1, 1, 2, 4);
        }

        private float DegreeToRadian(float angle)
        {
            return (float)(Math.PI * angle / 180.0);
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            Health.LoadContent();
            Health.Bounds = new Rectangle(20, 150, 256, 32);
            Mana.LoadContent();
            Mana.Bounds = new Rectangle(20, 200, 256, 32);

            PlayerSwordTexture = Game.Content.Load<Texture2D>("Sprites\\PlayerSword");
            PlayerNoneActionTexture = Game.Content.Load<Texture2D>("Sprites\\PlayerUnarmed");
            PlayerSwordAttackTexture = Game.Content.Load<Texture2D>("Sprites\\PlayerSwordAttack");

            ActionMode = Action.None;

            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Mana.Value = new Random().Next(0, 100);
                    Thread.Sleep(1000);
                }
            });
        }

        public void Update(GameTime gameTime)
        {
            particleEngine.EmitterLocation = GetBounds.Center.ToVector2() - new Vector2(0, 9);
            Health.Update(gameTime);
            Mana.Update(gameTime);

            base.Update(gameTime);
            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (ActionMode != Action.SwordAttack)
            {
                _playerControll.Update(gameTime);
            }
            var states = _playerControll.State;

            if (ActionMode != Action.SwordAttack)
            {
                if (_playerControll.IsMoving)
                {
                    _anim.AnimationTime = 100;
                }
                else
                {
                    _anim.AnimationTime = 550;
                }
            }

            if ((states.Contains(MovementState.Up) || states.Contains(MovementState.Down)) &&
                    (states.Contains(MovementState.Right) || states.Contains(MovementState.Left)))
            {
                Velocity = DiagonalSpeed;
            }
            else
            {
                Velocity = ConstSpeed;
            }


            if (states.Contains(MovementState.Up))
            {
                collision.Bounds = new Rectangle((int)Position.X + 4, (int)Position.Y + Height - 15, Width - 8,
                    16);
                if (collision.IsWalkableCollision(new Vector2(0, -((Velocity + 2000) * deltaTime)), GodMode))
                    Position.Y -= Velocity * deltaTime;
                Collis(MovementState.Up);
            }
            if (states.Contains(MovementState.Right))
            {
                collision.Bounds = new Rectangle((int)Position.X + 4, (int)Position.Y + Height - 15, Width - 8,
                    16);
                if (collision.IsWalkableCollision(new Vector2((Velocity + 2000) * deltaTime, 0), GodMode))
                    Position.X += Velocity * deltaTime;
                Collis(MovementState.Right);
            }
            if (states.Contains(MovementState.Left))
            {
                collision.Bounds = new Rectangle((int)Position.X + 4, (int)Position.Y + Height - 15, Width - 8,
                    16);
                if (collision.IsWalkableCollision(new Vector2(-((Velocity + 2000) * deltaTime), 0), GodMode))
                    Position.X -= Velocity * deltaTime;
                Collis(MovementState.Left);
            }
            if (states.Contains(MovementState.Down))
            {
                collision.Bounds = new Rectangle((int)Position.X + 4, (int)Position.Y + Height - 15, Width - 8,
                    16);
                if (collision.IsWalkableCollision(new Vector2(0, (Velocity + 2000) * deltaTime), GodMode))
                    Position.Y += Velocity * deltaTime;
                Collis(MovementState.Down);
            }


            if (ActionMode != Action.SwordAttack)
            {
                var mousePos =
                    new Vector2(
                        camera.AbsolutePosition.X - Game.GraphicsDevice.Viewport.Bounds.Width / 2 - Width / 2 +
                        Mouse.GetState().X,
                        camera.AbsolutePosition.Y - Game.GraphicsDevice.Viewport.Bounds.Height / 2 - Height / 2 +
                        Mouse.GetState().Y);

                var angleRadians = Math.Atan2(mousePos.Y - Position.Y, mousePos.X - Position.X);
                angle = (float)(angleRadians * (180 / Math.PI));
                if (angle < 0)
                {
                    angle = 360 - (-angle);
                }
                angle += 22.5f;
                if (angle >= 22.5f && angle < 45 || angle > 360)
                {
                    state = MovementState.Right;
                }
                if (angle >= 45 && angle < 90)
                {
                    state = MovementState.RightBottom;
                }
                if (angle >= 90 && angle < 135)
                {
                    state = MovementState.Down;
                }
                if (angle >= 135 && angle < 180)
                {
                    state = MovementState.DownLeft;
                }
                if (angle >= 180 && angle < 225)
                {
                    state = MovementState.Left;
                }
                if (angle >= 225 && angle < 270)
                {
                    state = MovementState.LeftUp;
                }
                if (angle >= 270 && angle < 315)
                {
                    state = MovementState.Up;
                }
                if (angle >= 315 && angle < 360)
                {
                    state = MovementState.UpRight;
                }
            }

            if ((_playerControll.IsMoving && ActionMode == Action.Sword) || ActionMode == Action.SwordAttack)
                particleEngine.Update(gameTime, true);
            else
                particleEngine.Update(gameTime, false);

            if (state == MovementState.Up && !_playerControll.IsMoving)
            {
                state = MovementState.IdleUp;
            }
            if (state == MovementState.UpRight && !_playerControll.IsMoving)
            {
                state = MovementState.IdleUpRight;
            }

            if (state == MovementState.Left && !_playerControll.IsMoving)
            {
                state = MovementState.IdleLeft;
            }
            if (state == MovementState.LeftUp && !_playerControll.IsMoving)
            {
                state = MovementState.IdleLeftUp;
            }

            if (state == MovementState.Right && !_playerControll.IsMoving)
            {
                state = MovementState.IdleRight;
            }
            if (state == MovementState.RightBottom && !_playerControll.IsMoving)
            {
                state = MovementState.IdleRightBottom;
            }

            if (state == MovementState.DownLeft && !_playerControll.IsMoving)
            {
                state = MovementState.IdleDownLeft;
            }
            if (state == MovementState.Down && !_playerControll.IsMoving)
            {
                state = MovementState.IdleDown;
            }


            _anim.Position = Position.ToVector2();
            _playerControll.State = states;

            particleEngine.Update(gameTime, newParticles);


            _anim.Update(gameTime);
            mouse.Update(gameTime);
        }



        private void Collis(MovementState _state)
        {
            _playerControll.State.Remove(_state);
        }

        public new void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(camera, SpriteSortMode.Deferred,
                BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null);

            if (ActionMode != Action.SwordAttack)
                _anim.Draw(spriteBatch, state);
            else
                _anim.Draw(spriteBatch, state, lastSwordAttack);

            spriteBatch.End();


            collision.Draw(spriteBatch);
            particleEngine.Draw(spriteBatch);
            _playerControll.IsMoving = false;


        }

        public new void DrawConditions(SpriteBatch spriteBatch)
        {
            Health.Draw(spriteBatch);
            Mana.Draw(spriteBatch);
        }

        private class PlayerWorker : GameComponent
        {
            private KeyboardControl Keyboard { get; }
            private MovementState ControlState { get; set; }
            public List<MovementState> State { get; set; }
            public bool IsMoving { get; set; }

            public PlayerWorker(Game game) : base(game)
            {
                State = new List<MovementState>();
                Keyboard = new KeyboardControl(game);

                Keyboard.PressedUp += PressedUp;
                Keyboard.PressedRight += PressedRight;
                Keyboard.PressedDown += PressedDown;
                Keyboard.PressedLeft += PressedLeft;
            }

            public override void Update(GameTime gameTime)
            {
                base.Update(gameTime);
                Keyboard.Update(gameTime);
            }

            public void PressedLeft(object sender, EventArgs e)
            {
                ControlState = MovementState.Left;
                State.Add(MovementState.Left);
                IsMoving = true;
            }

            public void PressedDown(object sender, EventArgs e)
            {
                ControlState = MovementState.Down;
                State.Add(MovementState.Down);
                IsMoving = true;
            }

            public void PressedRight(object sender, EventArgs e)
            {
                ControlState = MovementState.Right;
                State.Add(MovementState.Right);
                IsMoving = true;
            }

            public void PressedUp(object sender, EventArgs e)
            {
                ControlState = MovementState.Up;
                State.Add(MovementState.Up);
                IsMoving = true;
            }
        }
    }
}