﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PhysicGame.Objects.Bullets
{
    class Bullet : DrawableGameComponent
    {
        private Camera _camera;

        private Collision collisionMap;
        private SquareCollision collision;

        private Texture2D _texture;
        public Texture2D Texture
        {
            get { return _texture ?? (_texture = Game.Content.Load<Texture2D>(@"Textures\Default")); }
            set { _texture = value; }
        }

        public Point Size { get; set; } = new Point(20, 20);

        public float rotation { get; set; }

        public Vector2 Position { get; set; }
        public float Velocity = 200f;

        private Vector2 direction;

        public bool Shooted { get; set; }

        public Bullet(Game game, Camera camera, float rotation) : base(game)
        {
            this.rotation = rotation;
            this._camera = camera;
            //      collision = new SquareCollision();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {

            if (Shooted)
            {
                direction = new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation));
                Position += direction * Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

                // if (!collision.IsWalkableCollision(Velocity, false))
                // {
               // this.Dispose();
                //}
            }
           

            base.Update(gameTime);

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(_camera, SpriteSortMode.Deferred,
                BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null);
            var origin = new Vector2(Texture.Width/2, Texture.Height/2);
            if (Shooted)
                spriteBatch.Draw(Texture,
                    new Vector2((int)Position.X + (Texture.Width*2), (int)Position.Y - Texture.Height/2 + 8),
                    new Rectangle(0,0, Texture.Width, Texture.Height), Color.White,
                    rotation, origin,1, SpriteEffects.None, 1f);
            spriteBatch.End();
        }
    }
}
