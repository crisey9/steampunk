﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using Comora;
using GameLibrary.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PhysicGame.Objects
{
    public class Creature : DrawableGameComponent
    {
        public event EventHandler<PositionEventArgs> PositionChanged;
        public event EventHandler TileChanged;

        protected Camera camera;

        public int Width { get; set; } = 48;
        public int Height { get; set; } = 48;

        public int Velocity { get; set; }

        private Position _position;
        private Texture2D _texture;

        public Texture2D Texture
        {
            get => _texture ?? (_texture = Game.Content.Load<Texture2D>(@"Textures\Default"));
            set => _texture = value;
        }

        public Position Position
        {
            get => _position;
            set
            {
                _position = value;
            }
        }

        public Rectangle GetBounds => new Rectangle((int)Position.X, (int)Position.Y, Width, Height);

        public Creature(Game game, Camera camera) : base(game)
        {
            Velocity = 0;
        //    _position = new Position(100, 100);
            this.camera = camera;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(camera);
            spriteBatch.Draw(Texture, GetBounds, Color.White);
            spriteBatch.End();
        }

        protected virtual void OnTileChanged(EventArgs e)
        {
            EventHandler eh = TileChanged;
            eh?.Invoke(this, e);
        }

        public virtual void OnPositionChanged(PositionEventArgs e)
        {
            EventHandler<PositionEventArgs> eh = PositionChanged;
            eh?.Invoke(this, e);
        }
    }
}
