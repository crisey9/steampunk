﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Graphic;
using GameLibrary.Map;
using GameLibrary.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PhysicGame.Objects.Bullets;
using PhysicGame.Pathfinding;

namespace PhysicGame.Objects
{
    public class CannonMonster : Creature
    {
        private List<Bullet> bullets;

        private double acctuallyDelay = 0;
        public float AttackDelay { get; set; } = 1500;
        private Animator _anim;
        private Map map;
        private float angleRadians;
        public float angle;
        private Player _player;
        private MovementState state;

        public Point AttackDistance { get; set; } = new Point(325, 325);

        private SquareCollision collision;

        private Texture2D bulletTexture;

        public CannonMonster(Game game, Map map, Camera camera, Player player) : base(game, camera)
        {
            Width = 48;
            Height = 48;
            this._player = player;
            this.map = map;
            bullets = new List<Bullet>();
        }

        public void LoadContent()
        {
            base.LoadContent();
            Texture = Game.Content.Load<Texture2D>("Sprites/Enemies/Turret/Turret");
            bulletTexture = Game.Content.Load<Texture2D>("Textures/Bullet");
            collision = new SquareCollision(Game, _player, camera)
            {
                Bounds = new Rectangle((int)Position.X - (AttackDistance.X/2) + Width/2 , (int)Position.Y - (AttackDistance.Y / 2) + Height / 2, AttackDistance.X, AttackDistance.Y),
                SmallerCollision = Point.Zero,
                CollisionDraw = false
            };

        }

        public override void Initialize()
        {
            base.Initialize();
           
            _anim = new Animator(Game, Texture);
            _anim.Initialize();
            _anim.AnimationTime = 1000;
            _anim.HeightSingleSprite = 48;
            _anim.WidthSingleSprite = 48;
        }

        public override void Update(GameTime gameTime)
        {
            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            

            angleRadians = (float)Math.Atan2(_player.GetBounds.Center.Y - GetBounds.Center.Y, _player.GetBounds.Center.X - GetBounds.Center.X);
            angle = (float)(angleRadians * (180 / Math.PI));
            if (angle < 0)
            {
                angle = 360 - (-angle);
            }

            angle += 22.5f;
            if (angle >= 22.5f && angle < 45 || angle > 360)
            {
                state = MovementState.Right;
            }
            if (angle >= 45 && angle < 90)
            {
                state = MovementState.RightBottom;
            }
            if (angle >= 90 && angle < 135)
            {
                state = MovementState.Down;
            }
            if (angle >= 135 && angle < 180)
            {
                state = MovementState.DownLeft;
            }
            if (angle >= 180 && angle < 225)
            {
                state = MovementState.Left;
            }
            if (angle >= 225 && angle < 270)
            {
                state = MovementState.LeftUp;
            }
            if (angle >= 270 && angle < 315)
            {
                state = MovementState.Up;
            }
            if (angle >= 315 && angle < 360)
            {
                state = MovementState.UpRight;
            }
            _anim.Position = Position.ToVector2();

            if (collision.IsTriggered())
            {
                if (acctuallyDelay <= 0)
                    Shot();
            }

            if (acctuallyDelay > 0)
                acctuallyDelay -= gameTime.ElapsedGameTime.TotalMilliseconds ;

            foreach (var bullet in bullets)
            {
                bullet.Update(gameTime);
            }



            base.Update(gameTime);
            _anim.Update(gameTime);
        }

        private void Shot()
        {
            Bullet bullet = new Bullet(Game, camera, angleRadians);
            bullet.Position = new Vector2(Position.X - Width, Position.Y + Height /2 -8);
            bullet.Texture = bulletTexture;

            bullets.Add(bullet);
            Console.WriteLine(Position);
            acctuallyDelay = AttackDelay;
            bullet.Shooted = true;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(camera, SpriteSortMode.Deferred,
                BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null);

            _anim.TmpDraw(spriteBatch, state);

            spriteBatch.End();
            foreach (var bullet in bullets)
            {
                bullet.Draw(spriteBatch);
            }

            collision.Draw(spriteBatch);
        }
    }
}
