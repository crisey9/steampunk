﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using GameLibrary.Graphic.Particles;
using GameLibrary.UI.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Button = GameLibrary.UI.Controls.Button;
using Control = GameLibrary.UI.Core.Control;
using Label = GameLibrary.UI.Controls.Label;

namespace DevTool
{
    class ParticleEditor : DrawableGameComponent
    {
        private List<Texture2D> _texture2Ds;

        public List<Texture2D> Texture2Ds
        {
            get { return _texture2Ds; }
            set
            {
                _texture2Ds = value;
                particleEngine.Textures = value;
            }
        }

        private List<Textbox> sliders;
        private List<Label> labels;
        private int Margin = 15;
        private float longestText = 0;

        private Button submitBtn;
        private Button saveBtn;
        private Button loadFile;
        private Button loadAll;

        public ParticleEngine particleEngine;
        private ParticleEngineUtil particleUtil;

        private Rectangle _spawnLocation;

        public Rectangle SpawnLocation
        {
            get { return _spawnLocation; }
            set
            {
                _spawnLocation = value;
                particleEngine.EmitterLocation = value.Center.ToVector2();
                particleEngine.WidthRespawn = value.Width;
                particleEngine.HeightRespawn = value.Height;
            }
        }


        public ParticleEditor(Game game) : base(game)
        {
            particleEngine = new ParticleEngine(Texture2Ds, Game);
            particleUtil = new ParticleEngineUtil();
            Texture2Ds = new List<Texture2D>();
            sliders = new List<Textbox>();
            labels = new List<Label>();
            submitBtn = new Button(game);
            saveBtn = new Button(game);
            loadFile = new Button(game);
            loadAll = new Button(game);

            for (int i = 0; i < particleUtil.GetType().GetProperties().Count(); i++)
            {
                labels.Add(new Label(game));
                sliders.Add(new Textbox(game));
                sliders[i].Text.Color = Color.WhiteSmoke;
                sliders[i].Iterator = i;
                sliders[i].Tab += ParticleEditor_Tab;

                labels[i].Text.Value = particleUtil.GetType().GetProperties()[i].Name;

                if (labels[i].MeasureString.X > longestText)
                    longestText = labels[i].MeasureString.X;
            }
            submitBtn.MouseUp += OnSubmitParticle;
            saveBtn.Click += SaveToFile;
            loadFile.MouseUp += LoadFile;
            loadAll.MouseUp += LoadAll_MouseUp;
        }

        private void ParticleEditor_Tab(object sender, EventArgs e)
        {
            var tmp = (sender as Textbox);
            sliders[tmp.Iterator].IsSelected = false;
            if (sliders.Count > tmp.Iterator + 1)
                sliders[tmp.Iterator + 1].IsSelected = true;
            else
                sliders[0].IsSelected = true;

        }

        private void LoadAll_MouseUp(object sender, EventArgs e)
        {
            Texture2Ds = new List<Texture2D>();

            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            var path = Environment.CurrentDirectory + @"\Saves";
            folderBrowser.SelectedPath = path;
            folderBrowser.ShowNewFolderButton = false;
            DialogResult result = folderBrowser.ShowDialog();

            if (result == DialogResult.OK)
            {
                path = folderBrowser.SelectedPath;
                foreach (var file in Directory
                    .EnumerateFiles(path)
                    .Where(file => file.ToLower().EndsWith("png") || file.ToLower().EndsWith("jpg"))
                    .ToList())
                {
                    using (var stream = new FileStream(file, FileMode.Open))
                    {
                        Texture2D texture2d = Texture2D.FromStream(GraphicsDevice, stream);
                        Texture2Ds.Add(texture2d);
                    }

                }

                SetValueFromConfigToControls(path + @"/config.txt");
                OnSubmitParticle(null, EventArgs.Empty);
            }
            else if (result == DialogResult.Cancel) ;
            else if (result == DialogResult.Abort) ;
        }

        private void LoadFile(object sender, EventArgs eventArgs)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            var path = Environment.CurrentDirectory + @"/Saves";
            openFileDialog.InitialDirectory = path;
            openFileDialog.RestoreDirectory = true;
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                SetValueFromConfigToControls(openFileDialog.FileName);
                OnSubmitParticle(null, EventArgs.Empty);
            }
            else if (result == DialogResult.Cancel) ;
            else if (result == DialogResult.Abort) ;
        }

        private void SetValueFromConfigToControls(string path)
        {
            var filePath = path;
            var text = File.ReadAllLines(filePath);
            for (var i = 0; i < text.Count(); i++)
            {
                var s = text[i];
                var index = s.LastIndexOf('=') + 2;
                var tmp = s.Substring(index);
                sliders[i].Text.Value = tmp;
            }
        }

        private void SaveToFile(object sender, EventArgs e)
        {
            var path = Environment.CurrentDirectory + @"/Saves/";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            var i = 0;
            while (Directory.Exists(path + i))
                i++;
            path += i + @"/";
            var toFile = path + @"config.txt";

            Directory.CreateDirectory(path);

            for (int j = 0; j < Texture2Ds.Count; j++)
            {
                Stream stream = File.Create(path + j + ".png");
                Texture2Ds[j].SaveAsPng(stream, Texture2Ds[j].Width, Texture2Ds[j].Height);

                stream.Dispose();
            }

            //CONFIG
            try
            {
                for (int j = 0; j < particleUtil.GetType().GetProperties().Count(); j++)
                    File.AppendAllText(toFile,
                        $"{particleUtil.GetType().GetProperties()[j].Name} = {Convert.ToInt32(sliders[j].Text.Value)}{Environment.NewLine}");
            }
            catch (Exception ex)
            {
                Directory.Delete(path, true);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }

        }

        public override void Initialize()
        {
            sliders[0].Bounds = new Rectangle(250, 25, 250, 40);
            labels[0].Text.Position = new Vector2(10 + (longestText - labels[0].MeasureString.X), 35);

            for (int i = 1; i < sliders.Count; i++)
            {
                labels[i].Text.Position = new Vector2(10 + (longestText - labels[i].MeasureString.X), labels[0].Text.Position.Y + (i * (sliders[0].Bounds.Height + Margin)));
                sliders[i].Bounds = PositionUnderControl(sliders[i - 1], 250, 40, Margin);
            }

            submitBtn.Initialize();
            saveBtn.Initialize();
            loadFile.Initialize();
            loadAll.Initialize();


            base.Initialize();
        }

        private void KeyboardHelper_PressedRefresh(object sender, EventArgs e)
        {

        }

        public void OnSubmitParticle(object sender, EventArgs e)
        {
            int j = 0;
            foreach (var textbox in sliders)
            {
                if (!string.IsNullOrEmpty(textbox.Text.Value))
                    j++;
            }
            if (j == sliders.Count)
            {
                PropertyInfo[] properties = typeof(ParticleEngineUtil).GetProperties(); //stackoverflow ty
                for (int i = 0; i < properties.Length; i++)
                {
                    try
                    {
                        var value = sliders[i].Text.Value;
                        if (value.All(char.IsDigit))
                        {
                            properties[i].SetValue(particleUtil, Convert.ToInt32(value));
                        }
                        else
                        {
                            properties[i].SetValue(particleUtil, Convert.ToDouble(value));
                        }


                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;

                        Console.WriteLine(properties[i].Name + ": " +
                                          ex.Message);
                        Console.ResetColor();
                    }
                }
                particleEngine.Util = particleUtil;
            }
        }

        public void LoadContent()
        {
            foreach (var slider in sliders)
            {
                slider.LoadContent();
            }

            submitBtn.LoadContent();
            saveBtn.LoadContent();
            loadFile.LoadContent();
            loadAll.LoadContent();

            base.LoadContent();
            submitBtn.Text.Value = "Submit";
            submitBtn.Bounds = new Rectangle(250 + sliders[0].Bounds.Width - submitBtn.Bounds.Width, sliders[sliders.Count - 1].Bounds.Bottom + 20, 0, 0);

            saveBtn.Text.Value = "Save";
            saveBtn.Bounds = new Rectangle(250 + sliders[0].Bounds.Width - submitBtn.Bounds.Width - saveBtn.Bounds.Width - Margin, sliders[sliders.Count - 1].Bounds.Bottom + 20, 0, 0);

            loadFile.Text.Value = "Load";
            loadFile.Bounds = new Rectangle(250 + sliders[0].Bounds.Width - submitBtn.Bounds.Width - saveBtn.Bounds.Width - loadFile.Bounds.Width - Margin * 2, sliders[sliders.Count - 1].Bounds.Bottom + 20, 0, 0);

            loadAll.Text.Value = "Load all";
            loadAll.Bounds = new Rectangle(250 + sliders[0].Bounds.Width - submitBtn.Bounds.Width - saveBtn.Bounds.Width - loadFile.Bounds.Width - Margin * 2,
                                            loadFile.Bounds.Y + loadFile.Bounds.Height + Margin, 0, 0);
        }

        public void Update(GameTime gameTime, bool isChecked = true)
        {
            particleEngine.Update(gameTime);
            if (isChecked)
            {
                foreach (var slider in sliders)
                {
                    slider.Update(gameTime);
                }
                submitBtn.Update(gameTime);
                saveBtn.Update(gameTime);
                loadFile.Update(gameTime);
                loadAll.Update(gameTime);
            }
            base.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch, bool isChecked = true)
        {
            particleEngine.Draw(spriteBatch);

            if (isChecked)
            {
                for (int i = 0; i < sliders.Count; i++)
                {
                    sliders[i].Draw(spriteBatch);
                    labels[i].Draw(spriteBatch);
                }
                submitBtn.Draw(spriteBatch);
                saveBtn.Draw(spriteBatch);
                loadFile.Draw(spriteBatch);
                loadAll.Draw(spriteBatch);
            }
        }

        private Rectangle PositionUnderControl(Control control, int width, int height, int Margin = 15)
        {
            return new Rectangle(control.Bounds.X, control.Bounds.Y + control.Bounds.Height + Margin, width, height);
        }

        public static implicit operator List<object>(ParticleEditor v)
        {
            throw new NotImplementedException();
        }
    }
}
