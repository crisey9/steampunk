﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using GameLibrary.Graphic;
using GameLibrary.Graphic.Particles;
using GameLibrary.UI.Controls;
using GameLibrary.UI.Controls.Groups;
using GameLibrary.UI.Core;
using GameLibrary.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DevTool
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private FramerateCounter fpsMeter;

        private List<ParticleEditor> particleEditor;
        private GraphicManager graphManager;
        private FontsUtil font;

        private ComboBox comboBox;
        private Button addBtn;
        private Button deleteBtn;
        private int Selected;
        private DragDropResizeBox resizeBox;
        private Checkbox checkbox;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphManager = new GraphicManager(this, graphics);
            graphManager.DebugMode = true;
            graphManager.PreferMultiSampling = false;
            graphManager.VSync = false;
            graphManager.WindowSize = new Point(1600, 1000);
         //   graphics.PreferMultiSampling = false;
            IsMouseVisible = true;
           // graphics.SynchronizeWithVerticalRetrace = false;
            graphics.ApplyChanges();
           //IsFixedTimeStep = false;
            font = new FontsUtil(this);
            

            comboBox = new ComboBox(this);
            addBtn = new Button(this);
            deleteBtn = new Button(this);
            resizeBox = new DragDropResizeBox(this);
            checkbox = new Checkbox(this);
        }


        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            fpsMeter = new FramerateCounter(this);
            particleEditor = new List<ParticleEditor> { new ParticleEditor(this) };
            var bound = Window.ClientBounds;
            fpsMeter.Position = new Vector2(bound.Width - font.MediumFont.MeasureString("FPS MIN: 9999,9").X - 25,
                                            bound.Height - 4 * font.MediumFont.MeasureString("FPS").Y - 4 * fpsMeter.Margin - 25);


            comboBox.Initialize();
            addBtn.Initialize();
            checkbox.Initialize();
            foreach (var editor in particleEditor)
                editor.Initialize();

            resizeBox.BoundsChanged += ResizeBox_BoundsChanged;
            base.Initialize();
        }

        private void ResizeBox_BoundsChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(comboBox.Text.Value) && Convert.ToInt32(comboBox.Text.Value) > 0)
                particleEditor[Selected].SpawnLocation = resizeBox.Bounds;
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            var path = Environment.CurrentDirectory + @"/Particles";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            foreach (var file in Directory
                    .EnumerateFiles(path)
                    .Where(file => file.ToLower().EndsWith("png") || file.ToLower().EndsWith("jpg"))
                    .ToList())
            {
                using (var stream = new FileStream(file, FileMode.Open))
                {
                    Texture2D texture2d = Texture2D.FromStream(GraphicsDevice, stream);
                    foreach (var editor in particleEditor)
                        editor.Texture2Ds.Add(texture2d);
                }

            }

            foreach (var editor in particleEditor)
                editor.LoadContent();
            var bound = Window.ClientBounds;

            comboBox.LoadContent();
            comboBox.UpsideDown = true;
            comboBox.AddChildren(new ComboBoxItem(this, "1"));
            comboBox.Text.Value = "1";
            comboBox.Bounds = new Rectangle((int)fpsMeter.Position.X - 225, (int)fpsMeter.Position.Y, 200, 40);

            deleteBtn.LoadContent();
            deleteBtn.Text.Value = "Delete";
            deleteBtn.Bounds = new Rectangle(comboBox.Bounds.X, comboBox.Bounds.Y + 10 + comboBox.Bounds.Height, 0, 0);

            addBtn.LoadContent();
            addBtn.Text.Value = "Add";
            addBtn.Bounds = new Rectangle(comboBox.Bounds.X + deleteBtn.Bounds.Width + 10, deleteBtn.Bounds.Y, 0, 0);

            comboBox.Text.ValueChanged += (sender, args) =>
            {
                if (!string.IsNullOrEmpty(comboBox.Text.Value) && Convert.ToInt32(comboBox.Text.Value) > 0)
                {
                    Selected = Convert.ToInt32(comboBox.Text.Value) - 1;
                    particleEditor[Selected].OnSubmitParticle(null, EventArgs.Empty);
                }
            };

            addBtn.Click += AddBtn_Click;
            deleteBtn.Click += DeleteBtn_Click;

            resizeBox.LoadContent();
            resizeBox.Bounds = new Rectangle(600, 20, 800, 800);

            checkbox.LoadContent();
            checkbox.Text.Value = "Controls";
            checkbox.Bounds = new Rectangle(0, Window.ClientBounds.Bottom - 70, 40, 40);
            checkbox.IsChecked = true;
            // TODO: use this.Content to load your game content here
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(comboBox.Text.Value))
                particleEditor.RemoveAt(Convert.ToInt32(comboBox.Text.Value) - 1);
            comboBox.DeleteChildren(comboBox.Text.Value);
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            comboBox.AddChildren(new ComboBoxItem(this, (comboBox.Items.Count + 1).ToString()));
            particleEditor.Add(new ParticleEditor(this));
            particleEditor.Last().Initialize();
            var path = Environment.CurrentDirectory + @"/Particles";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            foreach (var file in Directory
                    .EnumerateFiles(path)
                    .Where(file => file.ToLower().EndsWith("png") || file.ToLower().EndsWith("jpg"))
                    .ToList())
            {
                using (var stream = new FileStream(file, FileMode.Open))
                {
                    Texture2D texture2d = Texture2D.FromStream(GraphicsDevice, stream);
                    particleEditor.LastOrDefault()?.Texture2Ds.Add(texture2d);
                }
            }
            particleEditor.LastOrDefault()?.LoadContent();
            var lastOrDefault = particleEditor.LastOrDefault();
            if (lastOrDefault != null) lastOrDefault.SpawnLocation = resizeBox.Bounds;
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }


        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            if (checkbox.IsChecked)
            {
                if (particleEditor.Count > 0)
                    particleEditor[Selected].Update(gameTime);

                comboBox.Update(gameTime);
                addBtn.Update(gameTime);
                deleteBtn.Update(gameTime);
                resizeBox.Update(gameTime);
            }
            else
            {
                if (particleEditor.Count > 0)
                    particleEditor[Selected].Update(gameTime, false);
            }
            checkbox.Update(gameTime);
            fpsMeter.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            if (checkbox.IsChecked)
            {
                if (particleEditor.Count > 0)
                    particleEditor[Selected].Draw(spriteBatch);

                comboBox.Draw(spriteBatch);
                addBtn.Draw(spriteBatch);
                deleteBtn.Draw(spriteBatch);
                resizeBox.Draw(spriteBatch, graphManager.DebugMode);
            }
            else
                if (particleEditor.Count > 0)
                particleEditor[Selected].Draw(spriteBatch, false);
            checkbox.Draw(spriteBatch);
            fpsMeter.Draw(spriteBatch);

            base.Draw(gameTime);
        }


    }
}
