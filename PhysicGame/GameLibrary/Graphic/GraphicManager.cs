﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;


namespace GameLibrary.Graphic
{
    public class GraphicManager : GameComponent
    {
        private GraphicsDeviceManager graphics;

        public GraphicManager(Game game, GraphicsDeviceManager graphics) : base(game)
        {
            this.graphics = graphics;
        }

        #region properties
        private Point _windowSize;
        private bool _isFullScreen = false;
        private bool _vSync = true;
        private bool _preferMultiSampling = true;
        public bool DebugMode { get; set; } = false;

        public Point WindowSize
        {
            get { return _windowSize; }
            set
            {
                _windowSize = value;
                graphics.PreferredBackBufferWidth = value.X;
                graphics.PreferredBackBufferHeight = value.Y;
                graphics.ApplyChanges();
            }
        }

        public bool IsFullScreen
        {
            get { return _isFullScreen; }
            set
            {
                _isFullScreen = value;
                graphics.IsFullScreen = value;
                graphics.ApplyChanges();
            }
        }

        public bool VSync
        {
            get { return _vSync; }
            set
            {
                _vSync = value;
                graphics.SynchronizeWithVerticalRetrace = value;
                Game.IsFixedTimeStep = value;
                graphics.ApplyChanges();
            }
        }

        public bool PreferMultiSampling
        {
            get { return _preferMultiSampling; }
            set
            {
                _preferMultiSampling = value;
                graphics.PreferMultiSampling = value;
                graphics.ApplyChanges();
            }
        }

        

        #endregion properties
    }
}
