﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Comora;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameLibrary.Utils;

namespace GameLibrary.Graphic.Particles
{
    public class Particle : DrawableGameComponent
    {
        public Texture2D Texture { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public float Angle { get; set; }
        public float AngularVelocity { get; set; }
        public Color Color { get; set; }
        public float Size { get; set; }
        public double TimeLife { get; set; }
        private double _spawnDelay;
        public float Scale { get; set; }= 1;

        public double SpawnDelay
        {
            get { return _spawnDelay; }
            set
            {
                _spawnDelay = value;
                if (SpawnDelay <= 0)
                    Visible = true;
            }
        }

        private readonly ThreadSafeRandom _rnd;

        public Particle(Texture2D texture, Vector2 position, Vector2 velocity,
            float spawnDelay, float angle, float angularVelocity, Color color, float size, int ttl, Game game) : base(game)
        {
            SpawnDelay = spawnDelay;
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            Color = color;
            Size = size;
            TimeLife = ttl;
            _rnd = new ThreadSafeRandom();
            Visible = false;
        }

        public void Update(GameTime gameTime, Action particleBehavior)
        {
            base.Update(gameTime);

            if (particleBehavior != null)
                particleBehavior();
            else
                DefaultParticleBehavior();
            var deltaTime = gameTime.ElapsedGameTime.TotalMilliseconds;

            if (SpawnDelay <= 0)
                TimeLife -= deltaTime;
            else
                SpawnDelay -= deltaTime;
            Position += new Vector2(Velocity.X * (float)deltaTime, Velocity.Y * (float)deltaTime);
            Angle += AngularVelocity * (float)deltaTime;
        }

        private void DefaultParticleBehavior()
        {
              if (TimeLife <= 400)
              {
              //    Color *= 0.225f;
                  if (Scale > 0.5f)
                  {
                      Scale -= .00125f;
                  }
              }

             /* if (Color.A <= 20)
              {
                  TimeLife = 0;
              }*/
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            var sourceRectangle = new Rectangle(0, 0, (int)Size, (int)Size);
            var origin = new Vector2(Texture.Width / 2, Texture.Height / 2);

            if (Visible)
                spriteBatch.Draw(Texture, Position, sourceRectangle, Color,
                    Angle, origin, Scale, SpriteEffects.None, 0f);
        }
    }
}
