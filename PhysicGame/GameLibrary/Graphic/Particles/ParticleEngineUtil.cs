﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary.Graphic.Particles
{
    public class ParticleEngineUtil
    {
        public double TimeLife { get; set; }
        public double TimeLifeRnd { get; set; }

        public double SizeMinRnd { get; set; }
        public double SizeMaxRnd { get; set; }

        public double AngleMinRnd { get; set; }
        public double AngleMaxRnd { get; set; }

        public double AngularVelocityMinRnd { get; set; }
        public double AngularVelocityMaxRnd { get; set; }

        public double VelocityXMinRnd { get; set; }
        public double VelocityXMaxRnd { get; set; }

        public double VelocityYMinRnd { get; set; }
        public double VelocityYMaxRnd { get; set; }

        public double MinParticles { get; set; }
        public double MaxParticles { get; set; }

        public double SpawnDelayRnd { get; set; }


        public ParticleEngineUtil()
        {
            
        }

        public ParticleEngineUtil(double spawnDelayRnd, double timeLife, double timeLifeRnd, double sizeMinRnd, double sizeMaxRnd, double angleMinRnd, double angleMaxRnd, double angularVelocityMinRnd, double angularVelocityMaxRnd, double velocityXMinRnd, double velocityXMaxRnd, double velocityYMinRnd, double velocityYMaxRnd, double minParticles, double maxParticles)
        {
            SpawnDelayRnd = spawnDelayRnd;
            TimeLife = timeLife;
            TimeLifeRnd = timeLifeRnd;
            SizeMinRnd = sizeMinRnd;
            SizeMaxRnd = sizeMaxRnd;
            AngleMinRnd = angleMinRnd;
            AngleMaxRnd = angleMaxRnd;
            AngularVelocityMinRnd = angularVelocityMinRnd;
            AngularVelocityMaxRnd = angularVelocityMaxRnd;
            VelocityXMinRnd = velocityXMinRnd;
            VelocityXMaxRnd = velocityXMaxRnd;
            VelocityYMinRnd = velocityYMinRnd;
            VelocityYMaxRnd = velocityYMaxRnd;
            MinParticles = minParticles;
            MaxParticles = maxParticles;
        }
    }
}
