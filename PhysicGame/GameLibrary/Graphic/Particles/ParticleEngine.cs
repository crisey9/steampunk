﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Graphic.Particles;
using GameLibrary.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphic.Particles
{
    public class ParticleEngine : DrawableGameComponent
    {
        private Camera _camera;
        private List<Particle> particles;
        private ThreadSafeRandom rnd;
        private ParticleEngineUtil _util;

        public Vector2 EmitterLocation { get; set; }
        public int WidthRespawn { get; set; }
        public int HeightRespawn { get; set; }

        public List<Texture2D> Textures;
        public ParticleEngineUtil Util
        {
            get { return _util; }
            set
            {
                _util = value;
                particles = new List<Particle>();
            }
        }

        public ParticleEngine(List<Texture2D> textures, Game game, Camera camera) : base(game)
        {
            _camera = camera;
            Util = new ParticleEngineUtil(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            this.Textures = textures;
            this.particles = new List<Particle>();
            rnd = new ThreadSafeRandom();
        }

        public void Update(GameTime gameTime, bool addNewParticles = true)
        {
            double count = rnd.NextDouble(
                Util.MinParticles - particles.Count,
                Util.MaxParticles - particles.Count);


            if (addNewParticles)
                for (int i = 0; i < count; i++)
                {
                    if (Textures.Count > 0)
                        particles.Add(GenerateNewParticle());
                }

            for (int particle = 0; particle < particles.Count; particle++)
            {

                particles[particle].Update(gameTime, null);
                if (particles[particle].TimeLife <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
            // Console.WriteLine(DateTime.Now);
        }

        private Particle GenerateNewParticle()
        {
            Texture2D texture = Textures[rnd.Next(Textures.Count)];
            if (WidthRespawn < 0)
                WidthRespawn = -WidthRespawn;
            if (HeightRespawn < 0)
                HeightRespawn = -HeightRespawn;

            Vector2 position = new Vector2(
                EmitterLocation.X + (float)rnd.NextDouble(-(WidthRespawn / 2), WidthRespawn / 2),
                EmitterLocation.Y + (float)rnd.NextDouble(-(HeightRespawn / 2), HeightRespawn / 2));

            Vector2 velocity = new Vector2((float)rnd.NextDouble((float)Util.VelocityXMinRnd, (float)Util.VelocityXMaxRnd),
                                           (float)rnd.NextDouble((float)Util.VelocityYMinRnd, (float)Util.VelocityYMaxRnd)) / 100;

            double angle = rnd.NextDouble((float)Util.AngleMinRnd, (float)Util.AngleMaxRnd) / 100;
            double angularVelocity = rnd.NextDouble((float)Util.AngularVelocityMinRnd, (float)Util.AngularVelocityMaxRnd) / 100;
            Color color = Color.White;
            float size = (float)rnd.NextDouble(Util.SizeMinRnd, Util.SizeMaxRnd);
            int timelife = (int)Util.TimeLife + rnd.Next(Convert.ToInt32(Util.TimeLifeRnd));
            var spawnDelay = rnd.NextDouble(0, Util.SpawnDelayRnd);
            if (velocity.X > 0)
            {
                angle = -angle;
            }
            return new Particle(texture, position, velocity, (float)spawnDelay, (float)angle, (float)angularVelocity, color, size, timelife, Game);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(_camera, SpriteSortMode.Texture, BlendState.AlphaBlend);
            for (int index = 0; index < particles.Count; index++)
            {
                particles[index].Draw(spriteBatch);
            }
            spriteBatch.End();
        }
    }
}
