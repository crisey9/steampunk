﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphic
{
    public class TilesetManager : DrawableGameComponent
    {
        public Texture2D Texture { get; set; }
        public Point SizeEachTile { get; set; } = new Point(24, 24);

        public TilesetManager(Game game) : base(game)
        {

        }

        public Texture2D GetTexture(int x, int y)
        {
            //http://stackoverflow.com/questions/16137500/cropping-texture2d-on-all-sides-in-xna-c-sharp
            // Calculate the cropped boundary
            Rectangle newBounds = new Rectangle();
            newBounds.X = x * SizeEachTile.X;
            newBounds.Y = y * SizeEachTile.Y;
            newBounds.Width = SizeEachTile.X;
            newBounds.Height = SizeEachTile.Y;

            // Create a new texture of the desired size
            Texture2D croppedTexture = new Texture2D(GraphicsDevice, newBounds.Width, newBounds.Height);

            // Copy the data from the cropped region into a buffer, then into the new texture
            Color[] data = new Color[newBounds.Width * newBounds.Height];
            Texture.GetData(0, newBounds, data, 0, newBounds.Width * newBounds.Height);
            croppedTexture.SetData(data);
            return croppedTexture;
        }

    }
}
