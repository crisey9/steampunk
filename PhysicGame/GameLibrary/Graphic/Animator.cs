﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameLibrary.Map;
using GameLibrary.Movement;

namespace GameLibrary.Graphic
{
    public class Animator : DrawableGameComponent
    {
        public event EventHandler AnimationDone;

        private double _timer;
        private int _timerCount;

        public int WidthSingleSprite { get; set; } = 48;
        public int HeightSingleSprite { get; set; } = 48;
        public Vector2 Position { get; set; }
        public Texture2D Texture { get; set; }
        public double AnimationTime { get; set; }

        public Animator(Game game) : base(game)
        {

        }

        public Animator(Game game, Texture2D texture) : base(game)
        {
            Texture = texture;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            _timer += gameTime.ElapsedGameTime.TotalMilliseconds;

            if (!(_timer >= AnimationTime)) return;
            var count = Texture.Width / WidthSingleSprite;
            if (count - 1 > _timerCount)
                _timerCount++;
            else
            {
                OnAnimationDone(EventArgs.Empty);
                _timerCount = 0;
            }
            _timer = 0;
        }

        public void Draw(SpriteBatch spriteBatch, MovementState state)
        {
                if (state == MovementState.Down)
                    DrawAnim(0 * 48, spriteBatch);
                else if (state == MovementState.RightBottom)
                    DrawAnim(1 * 48, spriteBatch);

                else if (state == MovementState.Right)
                    DrawAnim(2 * 48, spriteBatch);
                else if (state == MovementState.UpRight)
                    DrawAnim(3 * 48, spriteBatch);

                else if (state == MovementState.Up)
                    DrawAnim(4 * 48, spriteBatch);
                else if (state == MovementState.LeftUp)
                    DrawAnim(5 * 48, spriteBatch);

                else if (state == MovementState.Left)
                    DrawAnim(6 * 48, spriteBatch);
                else if (state == MovementState.DownLeft)
                    DrawAnim(7 * 48, spriteBatch);



                else if (state == MovementState.IdleDown)
                    DrawAnim(8 * 48, spriteBatch);
                else if (state == MovementState.IdleRightBottom)
                    DrawAnim(9 * 48, spriteBatch);

                else if (state == MovementState.IdleRight)
                    DrawAnim(10 * 48, spriteBatch);
                else if (state == MovementState.IdleUpRight)
                    DrawAnim(11 * 48, spriteBatch);

                else if (state == MovementState.IdleUp)
                    DrawAnim(12 * 48, spriteBatch);
                else if (state == MovementState.IdleLeftUp)
                    DrawAnim(13 * 48, spriteBatch);

                else if (state == MovementState.IdleLeft)
                    DrawAnim(14 * 48, spriteBatch);
                else if (state == MovementState.IdleDownLeft)
                    DrawAnim(15 * 48, spriteBatch);
        }


        public void Draw(SpriteBatch spriteBatch, MovementState state, bool swordAttackType)
        {
            if (!swordAttackType)
            {
                if (state == MovementState.Down || state == MovementState.IdleDown)
                    DrawAnim(0 * 48, spriteBatch);
                else if (state == MovementState.RightBottom || state == MovementState.IdleRightBottom)
                    DrawAnim(2 * 48, spriteBatch);

                else if (state == MovementState.Right || state == MovementState.IdleRight)
                    DrawAnim(4 * 48, spriteBatch);
                else if (state == MovementState.UpRight || state == MovementState.IdleUpRight)
                    DrawAnim(6 * 48, spriteBatch);

                else if (state == MovementState.Up || state == MovementState.IdleUp)
                    DrawAnim(8 * 48, spriteBatch);
                else if (state == MovementState.LeftUp || state == MovementState.IdleLeftUp)
                    DrawAnim(10 * 48, spriteBatch);

                else if (state == MovementState.Left || state == MovementState.IdleLeft)
                    DrawAnim(12 * 48, spriteBatch);
                else if (state == MovementState.DownLeft || state == MovementState.IdleDownLeft)
                    DrawAnim(14 * 48, spriteBatch);
            }
            else
            {
                if (state == MovementState.Down || state == MovementState.IdleDown)
                    DrawAnim(1 * 48, spriteBatch);
                else if (state == MovementState.RightBottom || state == MovementState.IdleRightBottom)
                    DrawAnim(3 * 48, spriteBatch);

                else if (state == MovementState.Right || state == MovementState.IdleRight)
                    DrawAnim(5 * 48, spriteBatch);
                else if (state == MovementState.UpRight || state == MovementState.IdleUpRight)
                    DrawAnim(7 * 48, spriteBatch);

                else if (state == MovementState.Up || state == MovementState.IdleUp)
                    DrawAnim(9 * 48, spriteBatch);
                else if (state == MovementState.LeftUp || state == MovementState.IdleLeftUp)
                    DrawAnim(11 * 48, spriteBatch);

                else if (state == MovementState.Left || state == MovementState.IdleLeft)
                    DrawAnim(13 * 48, spriteBatch);
                else if (state == MovementState.DownLeft || state == MovementState.IdleDownLeft)
                    DrawAnim(15 * 48, spriteBatch);
            }
        }

        public void TmpDraw(SpriteBatch spriteBatch, MovementState state)
        {
            if (state == MovementState.Down)
                DrawAnim(4 * 48, spriteBatch);
            else if (state == MovementState.RightBottom)
                DrawAnim(5 * 48, spriteBatch);

            else if (state == MovementState.Right)
                DrawAnim(6 * 48, spriteBatch);
            else if (state == MovementState.UpRight)
                DrawAnim(7 * 48, spriteBatch);

            else if (state == MovementState.Up)
                DrawAnim(0 * 48, spriteBatch);
            else if (state == MovementState.LeftUp)
                DrawAnim(1 * 48, spriteBatch);

            else if (state == MovementState.Left)
                DrawAnim(2 * 48, spriteBatch);
            else if (state == MovementState.DownLeft)
                DrawAnim(3 * 48, spriteBatch);
        }


        private void DrawAnim(int y, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture,
                   new Rectangle((int)Position.X, (int)Position.Y, WidthSingleSprite, HeightSingleSprite),
                   new Rectangle(_timerCount * WidthSingleSprite, y, WidthSingleSprite, HeightSingleSprite),
                   Color.White);
        }

        protected virtual void OnAnimationDone(EventArgs e)
        {
            EventHandler eh = AnimationDone;
            eh?.Invoke(this, e);
        }
    }
}
