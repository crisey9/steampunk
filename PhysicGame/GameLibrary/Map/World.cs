﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comora;
using GameLibrary.Graphic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Map
{
    public class World : DrawableGameComponent
    {
        public Map Map { get; set; }
        public string Path { get; set; }

        private int _tileSize;

        public int TileSize
        {
            get { return _tileSize; }
            set
            {
                _tileSize = value;
                Map = new Map(Game) { TileSize = TileSize };
            }
        }

        public World(Game game, string path) : base(game)
        {
            Path = path;
            Map = new Map(Game) { TileSize = TileSize };
        }

        public World(Game game) : base(game)
        {
            Map = new Map(Game) { TileSize = TileSize };
        }

        protected override void LoadContent()
        {
            base.LoadContent();

        }

        public override void Initialize()
        {
            base.Initialize();
            if (!string.IsNullOrEmpty(Path))
                Map.Initialize(Path);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        public void Draw(SpriteBatch spriteBatch, Camera _camera, Rectangle boundsPlayer, bool debugMode)
        {
            spriteBatch.Begin(_camera, SpriteSortMode.Deferred,
                BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null);
            foreach (var column in Map.TileManager.Rows.SelectMany(row => row.Columns))
            {
                    column.Tile.Draw(spriteBatch, debugMode);
            }


            spriteBatch.End();
        }
    }
}