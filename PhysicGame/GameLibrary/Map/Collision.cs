﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Graphic;

namespace GameLibrary.Map
{
    //Todo: add collision only near obj.
    public class Collision
    {
        public List<TileManager.Tile> CollisionTiles { get; set; }

        public Collision()
        {
            CollisionTiles = new List<TileManager.Tile>();
        }
    }

    public enum CollisionType
    {
        None,
        Block,
    }

    public enum CollisionDirection
    {
        Horizontal,
        Vertical
    }
}
