﻿using System;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Xml;
using GameLibrary.Graphic;
using Microsoft.Xna.Framework;

namespace GameLibrary.Map
{
    public class Map : GameComponent
    {
        private const string RegexTile = @"{(\d*):(\d*)\|(\d*)}";
        private TileManager.Tile _tile;
        public TileManager TileManager { get; set; }
        public int TileSize { get; set; }
        public string Path { get; set; }
        public Collision CollisionMap { get; set; }

        public Map(Game game) : base(game)
        {
            CollisionMap = new Collision();
        }

        public void Initialize(string path)
        {
            base.Initialize();
            _tile = new TileManager.Tile(Game);
            TileManager = new TileManager(Game) { Rows = new Collection<TileManager.Row>() };

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);

            XmlNode node = xmlDoc.SelectSingleNode("/Map/MapConfig/Size");
            int size = Convert.ToInt32(node.InnerText);

            XmlNodeList nodes = xmlDoc.SelectNodes("/Map/Layer/Row");
            Regex rgx = new Regex(RegexTile);

            for (int i = 0; i < size; i++)
            {
                TileManager.Rows.Add(new TileManager.Row());
                TileManager.Rows[i].Columns = new Collection<TileManager.Column>();

                for (int j = 0; j < size; j++)
                {
                    TileManager.Rows[i].Columns.Add(new TileManager.Column());
                    TileManager.Rows[i].Columns[j].Tile = new TileManager.Tile(Game);
                    _tile = TileManager.Rows[i].Columns[j].Tile;

                    var matches = rgx.Matches(nodes[i].InnerText);
                    _tile.TitlePath = "Textures/" + matches[j].Groups[1].Value;
                    //matches[j].Groups[3].Value - collision
                    //matches[j].Groups[2].Value - action
                    _tile.Bounds = new Rectangle(j * TileSize, i * TileSize, TileSize, TileSize);

                    if (matches[j].Groups[3].Value.Equals("1"))
                    {
                        _tile.CollisionType = CollisionType.Block;
                        CollisionMap.CollisionTiles.Add(_tile);
                    }
                    TileManager.Rows[i].Columns[j].Tile = _tile;
                }
            }
        }
    }
}