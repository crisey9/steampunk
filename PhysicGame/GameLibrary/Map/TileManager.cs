﻿using System;
using System.Collections.ObjectModel;
using GameLibrary.Map;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphic
{
    public class TileManager : DrawableGameComponent
    {
        public Collection<Row> Rows { get; set; }

        public TileManager(Game game) : base(game)
        {
            Rows = new Collection<Row>();
        }

        public class Row
        {
            public Collection<Column> Columns { get; set; }

            public Row()
            {
                Columns = new Collection<Column>();
            }
        }

        public class Column
        {
            public Tile Tile { get; set; }
        }

        public class Tile : DrawableGameComponent
        {
            private readonly FontsUtil _font;
            public Texture2D Texture { get; set; }
            public Rectangle Bounds { get; set; }
            public CollisionType CollisionType { get; set; } = CollisionType.None;
            public Point PosGet => new Point(Bounds.X / Bounds.Width, Bounds.Y / Bounds.Height);
            public bool Astar { get; set; } = false;


            private string _titlePath;
            public string TitlePath
            {
                get => _titlePath;
                set
                {
                    _titlePath = value;
                    try
                    {
                        Texture = Game.Content.Load<Texture2D>(_titlePath);
                    }
                    catch (ContentLoadException ex)
                    {
                        Console.WriteLine(ex.Message);
                        Texture = Game.Content.Load<Texture2D>("Textures/Default");
                    }
                }
            }

            public Tile(Game game) : base(game)
            {
                TitlePath = "Textures/Default";
                _font = new FontsUtil(game);
            }

            public Tile(Game game, Rectangle bounds, string titlePath = "Textures/Default") : base(game)
            {
                TitlePath = titlePath;
                Bounds = bounds;
                _font = new FontsUtil(game);
            }

            public void Draw(SpriteBatch spriteBatch, bool debugMode)
            {
                //spriteBatch.Begin();
                if (Astar)
                {
                    spriteBatch.Draw(Texture, Bounds, Color.LightGreen);
                }
                else if (debugMode && CollisionType == CollisionType.Block)
                {
                    spriteBatch.Draw(Texture, Bounds, Color.Red);
                   // spriteBatch.DrawString(_font.MediumFont, CollisionType.Block.ToString(), new Vector2(Bounds.X, Bounds.Y), Color.White);
                }
                else
                    spriteBatch.Draw(Texture, Bounds, Color.White);

                //spriteBatch.End();
            }
        }
    }
}
