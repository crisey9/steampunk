﻿using System;
using Microsoft.Xna.Framework;

namespace GameLibrary.Map
{
    public class Position
    {
        public event EventHandler ChangedPositionTile;

        protected virtual void OnChangedPositionTile(EventArgs e)
        {
            EventHandler eh = ChangedPositionTile;
            eh?.Invoke(this, e);
        }

        private float _x;

        public float X
        {
            get { return _x; }
            set
            {
                if ((int)value / 48 != (int)_x / 48)
                    OnChangedPositionTile(EventArgs.Empty);
                _x = value;
            }
        }

        private float _y;

        public float Y
        {
            get { return _y; }
            set
            {
                if ((int)value / 48 != (int)_y / 48)
                    OnChangedPositionTile(EventArgs.Empty);
                _y = value;
            }
        }


        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X, Y);
        }
    }

    public class PositionEventArgs : EventArgs
    {
        public Position Position { get; private set; }

        public PositionEventArgs(Position position)
        {
            Position = position;
        }
    }
}
