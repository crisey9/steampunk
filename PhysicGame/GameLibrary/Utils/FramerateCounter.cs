﻿using GameLibrary.Map;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Utils
{
    public class FramerateCounter : DrawableGameComponent
    {
        private FontsUtil _fonts;
        private double elapsedTime;

        private float _fps;
        private float _fpsMin = int.MaxValue;
        private float _fpsMax = int.MinValue;

        public int Interval { get; set; } = 175;
        public Vector2 Position { get; set; } = new Vector2(20, 20);
        public Color Color { get; set; } = Color.White;
        public int Margin { get; set; } = 0;

        public float Fps
        {
            get { return _fps; }
            set
            {
                _fps = value;
                if (FpsMax < value)
                    FpsMax = value;
                if (FpsMin > value)
                    FpsMin = value;
            }
        }
        public float FpsMin
        {
            get { return _fpsMin; }
            set
            {
                _fpsMin = value;
            }
        }
        public float FpsMax
        {
            get { return _fpsMax; }
            set
            {
                _fpsMax = value;
            }
        }

        public FramerateCounter(Game game) : base(game)
        {
            _fonts = new FontsUtil(game);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapsedTime >= Interval)
            {
                Fps = 1 / (float)gameTime.ElapsedGameTime.TotalSeconds;
                elapsedTime = 0;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            spriteBatch.DrawString(_fonts.MediumFont, $"FPS: {Fps:F1}", Position, Color);
            spriteBatch.DrawString(_fonts.MediumFont, $"FPS MIN: {FpsMin:F1}",
                new Vector2(Position.X, Position.Y + _fonts.MediumFont.MeasureString("FPS").Y + Margin), Color);
            spriteBatch.DrawString(_fonts.MediumFont, $"FPS MAX: {FpsMax:F1}",
                new Vector2(Position.X, Position.Y + _fonts.MediumFont.MeasureString("FPS").Y * 2 + Margin * 2), Color);
            spriteBatch.DrawString(_fonts.MediumFont, $"Sync: {Game.IsFixedTimeStep}",
                new Vector2(Position.X, Position.Y + _fonts.MediumFont.MeasureString("FPS").Y * 3 + Margin * 3), Color);

            spriteBatch.End();
        }
    }
}
