﻿using System;

namespace GameLibrary.Utils
{
    public class ThreadSafeRandom
    {
        private static readonly Random _global = new Random();
        [ThreadStatic]
        private static Random _local;

        public ThreadSafeRandom()
        {
            if (_local == null)
            {
                int seed;
                lock (_global)
                {
                    seed = _global.Next();
                }
                _local = new Random(seed);
            }
        }
        public int Next()
        {
            try
            {
                return _local.Next();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return 0;
        }
        public int Next(int max)
        {
            try
            {
                return _local.Next(max);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return 0;
        }
        public int Next(int min, int max)
        {
            try
            {
                return _local.Next(min, max);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return 0;
        }
        public double NextDouble()
        {
            try
            {
                return _local.NextDouble();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return 0;
        }

        public double NextDouble(int max)
        {
            try
            {
                return (double)_local.Next(max * 100) / 100;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return 0;
        }
        public double NextDouble(double min, double max)
        {
            try
            {
                double a = _local.Next((int)min * 100, (int)max * 100);
                double b = (double)a / 100;
                return b;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return 0;
        }
    }
}
