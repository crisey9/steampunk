﻿using System;
using GameLibrary.Graphic;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameLibrary.Mouse
{
    public class MouseInput1 : DrawableGameComponent
    {
        public Color Color { get; set; } = Microsoft.Xna.Framework.Color.Red;
        public int TileSize { get; set; } = 48;

        private Point _mousePositionTile;
        private Point _mousePosition;

        public event EventHandler MousePositionChanged;
        public event EventHandler MouseLeftClick;
        public event EventHandler MouseRightClick;
        public event EventHandler MouseDown;
        public event EventHandler MouseUp;


        private FontsUtil font;
        private MouseState _oldState;

        public Point MousePosition
        {
            get { return _mousePosition; }
            set
            {
                _mousePosition = value;
                OnMousePositionChanged(EventArgs.Empty);
            }
        }


        public Point MousePositionTile
        {
            get
            {
                int TileX = MousePosition.X / TileSize;
                int TileY = MousePosition.Y / TileSize;
                return new Point(TileX, TileY);
            }
            private set { _mousePositionTile = value; }
        }



        public MouseInput1(Game game) : base(game)
        {
            font = new FontsUtil(game);
        }

        public override void Update(GameTime gameTime)
        {

            MouseState newState = Microsoft.Xna.Framework.Input.Mouse.GetState();

            if (newState.Position != _oldState.Position)
            {
                MousePosition = newState.Position;
            }
            if (newState.LeftButton == ButtonState.Pressed && _oldState.LeftButton == ButtonState.Released)
            {
                OnMouseLeftClick(EventArgs.Empty);
            }
            if (newState.RightButton == ButtonState.Pressed && _oldState.RightButton == ButtonState.Released)
            {
                OnMouseRightClick(EventArgs.Empty);
            }
            if (newState.LeftButton == ButtonState.Pressed)
            {
                OnMouseDown(EventArgs.Empty);
            }
            if (newState.LeftButton == ButtonState.Released && _oldState.LeftButton == ButtonState.Pressed)
            {
                OnMouseUp(EventArgs.Empty);
            }

            _oldState = newState;

            base.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(font.MediumFont, MousePosition + " " + MousePositionTile, new Vector2(0, 0), Color);
            spriteBatch.End();
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 camera)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(font.MediumFont, "Absolute: " + MousePosition + " " + MousePositionTile + "   Relative: " +
                new Point(MousePosition.X + (int)camera.X, MousePosition.Y + (int)camera.Y) + " " +
                new Point((MousePosition.X + (int)camera.X) / TileSize, (MousePosition.Y + (int)camera.Y) / TileSize), new Vector2(0, 0), Color);

            spriteBatch.End();
        }

        protected virtual void OnMouseDown(EventArgs e)
        {
            EventHandler eh = MouseDown;
            eh?.Invoke(this, e);
        }

        protected virtual void OnMouseUp(EventArgs e)
        {
            EventHandler eh = MouseUp;
            eh?.Invoke(this, e);
        }

        protected virtual void OnMouseRightClick(EventArgs e)
        {
            EventHandler eh = MouseRightClick;
            eh?.Invoke(this, e);
        }

        protected virtual void OnMouseLeftClick(EventArgs e)
        {
            EventHandler eh = MouseLeftClick;
            eh?.Invoke(this, e);
        }

        protected virtual void OnMousePositionChanged(EventArgs e)
        {
            EventHandler eh = MousePositionChanged;
            eh?.Invoke(this, e);
        }
    }
}
