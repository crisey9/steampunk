﻿namespace GameLibrary.Movement
{
    public enum MovementState
    {
        IdleUp = 7,
        IdleRight = 6,
        IdleLeft = 5,
        IdleDown = 4,

        IdleUpRight = 12,
        IdleRightBottom = 13,
        IdleDownLeft = 14,
        IdleLeftUp = 15,

        Up = 3,
        Right = 2,
        Left = 1,
        Down = 0,

        UpRight = 8,
        RightBottom = 9,
        DownLeft = 10,
        LeftUp = 11,
    }
}