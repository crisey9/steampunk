﻿using System;
using GameLibrary.Mouse;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Core
{
    public abstract class Control : DrawableGameComponent
    {
        public Text Text { get; set; }
        public virtual Rectangle Bounds
        {
            get
            {
                return _bounds;
            }
            set
            {
                _bounds = value;
                OnBoundsChanged(EventArgs.Empty);
            }
        }
        protected FontsUtil Font;
        public Texture2D Texture
        {
            get { return _texture ?? (_texture = Game.Content.Load<Texture2D>("Textures/Default")); }
            set { _texture = value; }
        }
        protected Rectangle _bounds;
        private bool isMouseDown = false;
        private bool isMouseHover = false;
        private Texture2D _texture;
        public MouseInput1 _mouseInput;

        protected Control(Game game) : base(game)
        {
            _mouseInput = new MouseInput1(game);
            Text = new Text();
            Font = new FontsUtil(Game);

        }

        public override void Initialize()
        {
            //base.Initialize();
        }

        protected override void LoadContent()
        {

            base.LoadContent();

            _mouseInput.MouseLeftClick += OnMouseClick1;
            _mouseInput.MouseRightClick += OnMouseClick1;
            _mouseInput.MouseDown += (sender, args) => { if (IsInArea()) OnMouseDown(EventArgs.Empty); };
            _mouseInput.MouseUp += (sender, args) => { if (isMouseDown) OnMouseUp(EventArgs.Empty); };
        }
        public new virtual void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            _mouseInput.Update(gameTime);

            if (IsInArea())
                OnMouseHover(EventArgs.Empty);
            if (isMouseHover && !IsInArea())
                OnMouseLeave(EventArgs.Empty);

        }
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred,
                              BlendState.AlphaBlend,
                              SamplerState.PointClamp,
                              null, null, null, null);

            spriteBatch.Draw(Texture, Bounds, Color.White);
            spriteBatch.End();
        }

        public virtual void Draw(SpriteBatch spriteBatch, Color color)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred,
                  BlendState.AlphaBlend,
                  SamplerState.PointClamp,
                  null, null, null, null);
            spriteBatch.Draw(Texture, Bounds, color);
            spriteBatch.End();
        }
        protected bool IsInArea()
        {
            return _mouseInput.MousePosition.X > Bounds.X &&
                   _mouseInput.MousePosition.X < Bounds.X + Bounds.Width &&
                   _mouseInput.MousePosition.Y > Bounds.Y &&
                   _mouseInput.MousePosition.Y < Bounds.Y + Bounds.Height;
        }
        protected bool IsInArea(Rectangle bound)
        {
            return _mouseInput.MousePosition.X > bound.X &&
                   _mouseInput.MousePosition.X < bound.X + bound.Width &&
                   _mouseInput.MousePosition.Y > bound.Y &&
                   _mouseInput.MousePosition.Y < bound.Y + bound.Height;
        }

        protected virtual void OnMouseClick1(object sender, EventArgs e)
        {
            if (IsInArea())
                OnClick(EventArgs.Empty);
            else
                OnClickElseWhere(EventArgs.Empty);
        }

        public event EventHandler Click;
        public event EventHandler MouseHover;
        public event EventHandler MouseLeave;
        public event EventHandler MouseDown;
        public event EventHandler MouseUp;
        public event EventHandler BoundsChanged;
        public event EventHandler ClickElseWhere;

        private void OnMouseDown(EventArgs e)
        {
            isMouseDown = true;
            EventHandler eh = MouseDown;
            eh?.Invoke(this, e);
        }
        private void OnMouseUp(EventArgs e)
        {
            isMouseDown = false;
            EventHandler eh = MouseUp;
            eh?.Invoke(this, e);
        }
        private void OnMouseHover(EventArgs e)
        {
            isMouseHover = true;
            EventHandler eh = MouseHover;
            eh?.Invoke(this, e);
        }
        protected virtual void OnMouseLeave(EventArgs e)
        {
            isMouseHover = false;
            EventHandler eh = MouseLeave;
            eh?.Invoke(this, e);
        }
        protected virtual void OnClick(EventArgs e)
        {
            EventHandler eh = Click;
            eh?.Invoke(this, e);
        }
        protected void OnBoundsChanged(EventArgs e)
        {
            EventHandler eh = BoundsChanged;
            eh?.Invoke(this, e);
        }
        private void OnClickElseWhere(EventArgs e)
        {
            EventHandler eh = ClickElseWhere;
            eh?.Invoke(this, e);
        }
    }
}
