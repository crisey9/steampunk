﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameLibrary.UI.Core
{
    public class Text
    {
        public Vector2 Position { get; set; } = new Vector2(0, 0);
        public Color Color { get; set; } = Color.White;
        public event EventHandler ValueChanged;

        private string _value;

        public string Value
        {
            get
            {
                return string.IsNullOrEmpty(_value) ? string.Empty : _value;
            }
            set
            {
                _value = value;
                OnValueChanged(EventArgs.Empty);
            }
        }

        public Text()
        {

        }

        protected virtual void OnValueChanged(EventArgs e)
        {
            ValueChanged?.Invoke(this, e);
        }
    }
}
