﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Mouse;
using GameLibrary.UI.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Core
{
    public class Form : DrawableGameComponent
    {
        public Button CloseButton { get; set; }
        protected int TitleBarHeight { get; set; } = 25;
        public string Title { get; set; } = "Title";
        public bool IsTitleBar { get; set; } = true;
        public bool IsMovable { get; set; } = true;

        private readonly FontsUtil _fonts;
        private readonly List<Control> _controls;
        private readonly MouseInput1 _mouseInput;

        private Texture2D _titleBarTexture;
        private Texture2D _formTexture;
        private Rectangle _bounds;
        private bool isFocused = false;

        public Texture2D TitleBarTexture
        {
            get { return _titleBarTexture ?? (_titleBarTexture = Game.Content.Load<Texture2D>("Textures/Default")); }
            set { _titleBarTexture = value; }
        }
        public Texture2D FormTexture
        {
            get { return _formTexture ?? (_formTexture = Game.Content.Load<Texture2D>("Textures/Default")); }
            set { _formTexture = value; }
        }
        public Rectangle Bounds
        {
            get { return _bounds; }
            set
            {
                if (value.X + value.Width / 2 < Game.GraphicsDevice.Viewport.X ||
                    value.X > Game.GraphicsDevice.Viewport.Width - value.Width / 2 ||
                    value.Y - TitleBarHeight < Game.GraphicsDevice.Viewport.Y ||
                    value.Y > Game.GraphicsDevice.Viewport.Height) return;


                var tmp = CloseButton.Bounds;
                tmp.X = value.Center.X - (int)_fonts.MediumFont.MeasureString(CloseButton.Text.Value).X;
                Console.WriteLine((int)_fonts.MediumFont.MeasureString(CloseButton.Text.Value).X);
                tmp.Y = value.Bottom - CloseButton.Bounds.Height - 10;
                CloseButton.Bounds = tmp;
                Point differenceBounds = new Point(value.X - Bounds.X, value.Y - Bounds.Y);
                foreach (Control control in _controls)
                {
                    control.Bounds = new Rectangle(control.Bounds.X + differenceBounds.X,
                        control.Bounds.Y + differenceBounds.Y, control.Bounds.Width, control.Bounds.Height);
                }

                _bounds = value;
            }
        }

        private bool IsInTitleBarArea()
        {
            return _mouseInput.MousePosition.X > Bounds.X &&
                   _mouseInput.MousePosition.X < Bounds.X + Bounds.Width &&
                   _mouseInput.MousePosition.Y > Bounds.Y - TitleBarHeight &&
                   _mouseInput.MousePosition.Y < Bounds.Y;
        }
        private Rectangle ControlInFormRectangle(Control control)
        {
            return new Rectangle(Bounds.X + control.Bounds.X, Bounds.Y + control.Bounds.Y,
                                          control.Bounds.Width, control.Bounds.Height);
        }


        public Form(Game game) : base(game)
        {
            _controls = new List<Control>();
            _fonts = new FontsUtil(game);
            CloseButton = new Button(game);
            _mouseInput = new MouseInput1(game);
            _mouseInput.MouseDown += MouseInputOnMouseDown;
            _mouseInput.MouseUp += (sender, args) => { isFocused = false; };

            CloseButton.Click += (sender, args) => { Visible = false; };
        }


        private void MouseInputOnMouseDown(object sender, EventArgs eventArgs)
        {
            if (IsTitleBar && IsMovable)
            {
                if (IsInTitleBarArea())
                    isFocused = true;

                if (isFocused)
                {
                    Bounds = new Rectangle(_mouseInput.MousePosition.X - Bounds.Width / 2, _mouseInput.MousePosition.Y + TitleBarHeight / 2, Bounds.Width, Bounds.Height);
                }
            }
        }
        public void AddControl(Control control)
        {
            control.Bounds = ControlInFormRectangle(control);
            _controls.Add(control);
        }

        public void LoadContent()
        {
            // FormTexture = Game.Content.Load<Texture2D>("Textures/UI/Form/FormTexture");
            TitleBarTexture = Game.Content.Load<Texture2D>("Textures/UI/Form/TitleBarTexture");
            if (CloseButton.Bounds != new Rectangle())
                throw new SystemException("Set form bounds after ctor");
            CloseButton.LoadContent();
            CloseButton.AutoPosition = true;
            CloseButton.Text.Value = "Back";


            base.LoadContent();
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {

                spriteBatch.Begin();
                spriteBatch.Draw(FormTexture, Bounds, Color.White);

                if (IsTitleBar)
                {
                    spriteBatch.Draw(TitleBarTexture, new Rectangle(Bounds.X, Bounds.Y - TitleBarHeight, Bounds.Width, TitleBarHeight), Color.White);
                    spriteBatch.DrawString(_fonts.MediumFont, Title, new Vector2(
                        Bounds.X + Bounds.Width / 2 - _fonts.MediumFont.MeasureString(Title).Length() / 2,
                        Bounds.Y - TitleBarHeight / 2 - (_fonts.MediumFont.MeasureString(Title).Y / 2) + 1),
                        Color.White);
                }
                spriteBatch.End();
                foreach (Control control in _controls)
                {
                    control.Draw(spriteBatch);
                }
                if (CloseButton.Visible)
                    CloseButton.Draw(spriteBatch);
            }
        }
        public override void Update(GameTime gameTime)
        {
            _mouseInput.Update(gameTime);
            if (Visible)
            {
                foreach (Control control in _controls)
                {
                    control.Update(gameTime);
                }
                CloseButton.Update(gameTime);
                base.Update(gameTime);
            }
        }
    }
}
