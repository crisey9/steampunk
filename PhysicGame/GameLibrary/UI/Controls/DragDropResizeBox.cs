﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameLibrary.UI.Controls.Groups;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Control = GameLibrary.UI.Core.Control;

namespace GameLibrary.UI.Controls
{
    public class DragDropResizeBox : Control
    {
        public RectangleHolder LeftRectangle { get; set; }
        public RectangleHolder RightRectangle { get; set; }
        public Color Color { get; set; } = Color.Red;

        private Point _sizeRectangles;

        public Point SizeRectangles
        {
            get { return _sizeRectangles; }
            set
            {
                _sizeRectangles = value;
                LeftRectangle.Bounds = ResizeRectangle(LeftRectangle.Bounds, value);
                RightRectangle.Bounds = ResizeRectangle(RightRectangle.Bounds, value);
            }
        }

        private Rectangle ResizeRectangle(Rectangle rectangle, Point value)
        {
            Rectangle tmp = rectangle;
            tmp.Width = value.X;
            tmp.Height = value.Y;

            return tmp;
        }


        public DragDropResizeBox(Game game) : base(game)
        {
            LeftRectangle = new RectangleHolder(game);
            RightRectangle = new RectangleHolder(game);

            SizeRectangles = new Point(50, 50);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void LoadContent()
        {
            Texture = Game.Content.Load<Texture2D>("Textures/UI/box");

            BoundsChanged += DragDropResizeBox_BoundsChanged;

            LeftRectangle.LoadContent();
            RightRectangle.LoadContent();

            LeftRectangle.MouseDown += (sender, args) =>
            {
                LeftRectangle.IsDrag = true;
                LeftRectangle.Bounds = ChangePosition(LeftRectangle.Bounds);
            };
            LeftRectangle.MouseUp += RectanglesMouseUp;


            RightRectangle.MouseDown += (sender, args) =>
            {
                RightRectangle.IsDrag = true;
            };
            RightRectangle.MouseUp += RectanglesMouseUp;


            base.LoadContent();
        }

        private void RectanglesMouseUp(object sender, EventArgs e)
        {
            LeftRectangle.IsDrag = false;
            RightRectangle.IsDrag = false;
            var tmp = Bounds;
            var leftRec = LeftRectangle.Bounds;
            var rightRec = RightRectangle.Bounds;
            tmp.Width = rightRec.Right - leftRec.Left;
            tmp.X = leftRec.Left;
            tmp.Height = leftRec.Bottom - rightRec.Top;
            tmp.Y = rightRec.Top;
            Bounds = tmp;
            if (leftRec.Intersects(rightRec))
            {
                leftRec.X -= rightRec.Width - 20;
                leftRec.Y += rightRec.Height - 20;
            }
            LeftRectangle.Bounds = leftRec;
        }

        private Rectangle ChangePosition(Rectangle rec)
        {
            var tmp = rec;

            if (LeftRectangle.IsDrag || RightRectangle.IsDrag)
            {
                var position = _mouseInput.MousePosition;
                tmp.X = position.X - tmp.Width / 2;
                tmp.Y = position.Y - tmp.Height / 2;
            }
            return tmp;
        }

        private void DragDropResizeBox_BoundsChanged(object sender, EventArgs e)
        {
            LeftRectangle.Bounds = new Rectangle(Bounds.Left,
                                               Bounds.Bottom - SizeRectangles.Y,
                                               SizeRectangles.X,
                                               SizeRectangles.Y);
            RightRectangle.Bounds = new Rectangle(Bounds.Right - SizeRectangles.X,
                                                  Bounds.Top,
                                                  SizeRectangles.X,
                                                  SizeRectangles.Y);
        }



        public override void Update(GameTime gameTime)
        {
            LeftRectangle.Update(gameTime);
            RightRectangle.Update(gameTime);
            if (LeftRectangle.IsDrag)
                LeftRectangle.Bounds = ChangePosition(LeftRectangle.Bounds);
            if (RightRectangle.IsDrag)
                RightRectangle.Bounds = ChangePosition(RightRectangle.Bounds);

            base.Update(gameTime);

        }

        public void Draw(SpriteBatch spriteBatch, bool debugMode = true)
        {
            if (debugMode)
            {
                base.Draw(spriteBatch, Color.LightBlue);
                LeftRectangle.Draw(spriteBatch, Color);
                RightRectangle.Draw(spriteBatch, Color);
            }
        }
    }
}
