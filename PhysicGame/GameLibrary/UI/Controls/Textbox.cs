﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Windows.Forms;
using Control = GameLibrary.UI.Core.Control;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace GameLibrary.UI.Controls
{
    public class Textbox : Control
    {
        public int Margin { get; set; } = 15;
        public bool IsSelected { get; set; }
        public int Iterator { get; set; } = 0;

        private string _renderedValue;
        private Vector2 _textPos;

        private KeyboardState _oldState;
        private KeyboardState _newState;


        private double _timer;
        private double _firstTimer;

        public Textbox(Game game) : base(game)
        {

            Text.Color = Color.Black;
            _renderedValue = Text.Value;
            Click += (sender, args) =>
            {
                IsSelected = true;
            };
            ClickElseWhere += (sender, args) =>
            {
                IsSelected = false;
            };
            Text.ValueChanged += OnTextChanged;
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            _renderedValue = Text.Value;
            var boundsWidth = Bounds.Width - Margin;
            var widthFont = (int)Font.MediumFont.MeasureString(_renderedValue + "|").Length();

            while (widthFont >= boundsWidth)
            {
                _renderedValue = _renderedValue.Substring(1);
                widthFont = (int)Font.MediumFont.MeasureString(_renderedValue + "|").Length();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            /*textPos = new Vector2(Bounds.X - Margin + Bounds.Width - Font.MediumFont.MeasureString(_renderedValue).X, y: Bounds.Y + Bounds.Height / 2 - (Font.MediumFont.MeasureString(_renderedValue).Y / 2) + 1);*/

            _textPos = new Vector2(Bounds.X + Margin, Bounds.Y + Bounds.Height / 2 + -(Font.MediumFont.MeasureString(_renderedValue).Y / 2) + 1);
            spriteBatch.Begin();
            spriteBatch.DrawString(Font.MediumFont, _renderedValue, _textPos, Text.Color);
            if (IsSelected)
            {
                if (Text.Value.Length >= 1)
                    spriteBatch.DrawString(Font.MediumFont, "|",
                        new Vector2(Font.MediumFont.MeasureString(_renderedValue).X + (Margin / 2) + 3 + Bounds.X,
                            _textPos.Y), Text.Color);
                else
                    spriteBatch.DrawString(Font.MediumFont, "|",
                    new Vector2(Font.MediumFont.MeasureString(_renderedValue).X + Margin + Bounds.X,
                         Bounds.Y + Bounds.Height/4), Text.Color);
            }
            spriteBatch.End();
        }

        public new void LoadContent()
        {
            base.LoadContent();
            Texture = Game.Content.Load<Texture2D>("Textures/UI/Textbox");
        }

        public override void Update(GameTime gameTime)
        {
            
            base.Update(gameTime);
            if (!IsSelected) return;
            _newState = Keyboard.GetState();
            foreach (Keys key in Enum.GetValues(typeof(Keys)))
            {
                if (_newState.IsKeyDown(key) && _oldState.IsKeyUp(key))
                {
                    if (key == Keys.Enter) { OnSubmit(EventArgs.Empty); }
                    else if (key == Keys.Back && !string.IsNullOrEmpty(Text.Value)) Text.Value = Text.Value.Remove(Text.Value.Length - 1);
                    else if (key == Keys.Space) Text.Value += " ";
                    else if (key == Keys.OemTilde && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "~";
                    else if (key == Keys.OemTilde) Text.Value += "`";
                    else if (key == Keys.D1 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "!";
                    else if (key == Keys.D2 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "@";
                    else if (key == Keys.D3 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "#";
                    else if (key == Keys.D4 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "$";
                    else if (key == Keys.D5 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "%";
                    else if (key == Keys.D6 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "^";
                    else if (key == Keys.D7 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "&";
                    else if (key == Keys.D8 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "*";
                    else if (key == Keys.D9 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "(";
                    else if (key == Keys.D0 && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += ")";
                    else if (key == Keys.OemComma) Text.Value += ",";
                    else if (key == Keys.OemPeriod) Text.Value += ".";
                    else if (key == Keys.OemMinus) Text.Value += "-";
                    else if (key == Keys.OemPipe && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "|";
                    else if (key == Keys.OemPipe) Text.Value += @"\";
                    else if (key == Keys.OemOpenBrackets && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "{";
                    else if (key == Keys.OemCloseBrackets && IsSpecialChar(Keys.LeftShift, Keys.RightShift)) Text.Value += "}";
                    else if (key == Keys.OemOpenBrackets) Text.Value += "[";
                    else if (key == Keys.OemCloseBrackets) Text.Value += "]";
                    else if (key == Keys.C && IsSpecialChar(Keys.LeftControl, Keys.RightControl)) Clipboard.SetText(Text.Value);
                    else if (key == Keys.V && IsSpecialChar(Keys.LeftControl, Keys.RightControl)) Text.Value += Clipboard.GetText();
                    else if (key == Keys.Tab) OnTab(EventArgs.Empty);
                    else if (key == Keys.Delete) Text.Value = string.Empty;

                    else if (key >= Keys.D0 && key <= Keys.D9)
                        Text.Value += key.ToString().Substring(1);
                    else if (key >= Keys.NumPad0 && key <= Keys.NumPad9)
                        Text.Value += key.ToString().Substring(6);
                    else if (key.ToString().Length > 1) ;
                    else
                    {
                        var capsLockIsOn = System.Windows.Forms.Control.IsKeyLocked(System.Windows.Forms.Keys.CapsLock);
                        if (capsLockIsOn || IsSpecialChar(Keys.LeftShift, Keys.RightShift))
                            Text.Value += key.ToString();
                        else
                            Text.Value += key.ToString().ToLower();
                    }
                }
                else if (_newState.IsKeyDown(key))
                {
                    if (key != Keys.Back) continue;
                    if (string.IsNullOrEmpty(Text.Value)) continue;
                    _firstTimer += gameTime.ElapsedGameTime.TotalMilliseconds;
                    _timer += gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (!(_timer >= 30) || !(_firstTimer >= 475)) continue;
                    Text.Value = Text.Value.Remove(Text.Value.Length - 1);
                    _timer = 0;
                }
                else if (_newState.IsKeyUp(Keys.Back))
                {
                    _firstTimer = 0;
                }
            }
            _oldState = _newState;
        }

        private bool IsSpecialChar(params Keys[] keys)
        {
            var i = keys.Count(pKey => _newState.IsKeyDown(pKey));
            return (i >= keys.Length - 1);
        }


        public event EventHandler Submit;
        protected virtual void OnSubmit(EventArgs e)
        {
            EventHandler eh = Submit;
            eh?.Invoke(this, e);
        }


        public event EventHandler Tab;
        protected virtual void OnTab(EventArgs e)
        {
            EventHandler eh = Tab;
            eh?.Invoke(this, e);
        }
    }
}
