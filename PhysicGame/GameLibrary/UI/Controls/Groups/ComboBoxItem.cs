﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls.Groups
{
    public class ComboBoxItem : Control
    {
        public int Margin { get; set; } = 10;
        public bool IsUpsideDown { get; set; } = false;

        public ComboBoxItem(Game game, string value) : base(game)
        {
            Text.Value = value;
            BoundsChanged += (sender, args) =>
            {
                if (IsUpsideDown)
                    Text.Position = new Vector2(x: Bounds.X + Margin,
                                                y: Bounds.Y + (Bounds.Height / 2) - (Font.MediumFont.MeasureString(Text.Value).Y / 2) + 4);
                else
                    Text.Position = new Vector2(x: Bounds.X + Margin,
                                              y: Bounds.Y + (Bounds.Height / 2) - (Font.MediumFont.MeasureString(Text.Value).Y / 2) - 4);
            };

        }

        public void LoadContent()
        {
            base.LoadContent();
            // Text.Position = new Vector2(Bounds.X, Bounds.Y);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Begin();
            spriteBatch.DrawString(Font.MediumFont, Text.Value, Text.Position, Text.Color);
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);


        }
    }
}
