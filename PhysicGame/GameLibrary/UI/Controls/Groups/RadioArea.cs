﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.UI.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Core
{
    public class RadioArea : Control
    {
        private readonly List<Radiobox> _radioButtons;

        public RadioArea(Game game) : base(game)
        {
            _radioButtons = new List<Radiobox>();
        }

        public void AddToList(Radiobox radioButton)
        {
            radioButton.Click += RadioButton_Click;
            _radioButtons.Add(radioButton);
        }

        private void RadioButton_Click(object sender, EventArgs e)
        {
            foreach (var radioButton in _radioButtons.Where(x=>!x.Equals(sender)))
            {
                radioButton.IsChecked = false;
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var radioButton in _radioButtons)
            {
                radioButton.Update(gameTime);
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (var radioButton in _radioButtons)
            {
                radioButton.Draw(spriteBatch);
            }

            base.Draw(spriteBatch);
        }
    }
}
