﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls.Groups
{
    public class RectangleHolder : Control
    {
        public bool IsDrag { get; set; } = false;

        public RectangleHolder(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void LoadContent()
        {
            Texture = Game.Content.Load<Texture2D>("Textures/UI/box");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch, Color color)
        {
            base.Draw(spriteBatch, color);
        }
    }
}
