﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using GameLibrary.Mouse;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls
{
    public class Slidebar : Control
    {
        public int Margin { get; set; } = 15;

        private bool drag = false;
        public event EventHandler ValueChanged;

        private float ScaleHeight
        {
            get { return Bounds.Height/SliderSize.Y; }
        }

        private float Scale => MaxValue + (MinValue*-1);

        public float Value { get; set; } = 0;
        public float MaxValue { get; set; } = 500;
        public float MinValue { get; set; } = -400;
        public Point SliderSize { get; set; } = new Point(38, 20);
        public double ValuePercentage => ((Value - MinValue)/(MaxValue - MinValue))*100;

        /* public Rectangle PointerBounds
         {
             get { return new Rectangle(Bounds.X + Bounds.Width / 100 * (int)ValuePercentage - SliderSize.X / 2, Bounds.Y + Bounds.Height / 2 - SliderSize.Y/2, SliderSize.X, SliderSize.Y); }
             set { PointerBounds = value; }
         }*/

        private Rectangle _pointerBounds;

        public Rectangle PointerBounds
        {
            get { return _pointerBounds; }
            set { _pointerBounds = value; }
        }


        private Texture2D _slider;

        public Texture2D Slider
        {
            get { return _slider ?? Game.Content.Load<Texture2D>("Textures/Default"); }
            set { _slider = value; }
        }


        public Slidebar(Game game) : base(game)
        {
            BoundsChanged += (sender, args) =>
            {
                PointerBounds = new Rectangle(Bounds.X + Bounds.Width/2 - SliderSize.X/2,
                    Bounds.Y + Bounds.Height/2 - SliderSize.Y/2, SliderSize.X, SliderSize.Y);
            };
            //_mouseInput = new MouseInput(game);
            _mouseInput.MouseDown += MouseInputMouseInputDown;
            _mouseInput.MouseUp += MouseInputMouseInputUp;
        }

        private void MouseInputMouseInputUp(object sender, EventArgs e)
        {
            drag = false;
        }

        private void MouseInputMouseInputDown(object sender, EventArgs e)
        {
            if (IsInArea(PointerBounds))
                drag = true;

            if (drag)
            {
                if (_mouseInput.MousePosition.X > Bounds.X + 28 &&
                    _mouseInput.MousePosition.X < Bounds.X + Bounds.Width - 28)
                {
                    var tmp = PointerBounds;
                    tmp.X = _mouseInput.MousePosition.X - SliderSize.X/2;
                    PointerBounds = tmp;
                    // Console.WriteLine(Scale);
                    var skala = Scale/(Bounds.Width - (Margin*2) - (SliderSize.X/2) - 6);
                    float start = Bounds.X + 9;
                    Value = (PointerBounds.X - start)*skala;
                    OnValueChanged(EventArgs.Empty);
                    if (MinValue < 0)
                        Value -= Math.Abs(MinValue);
                    else if (MinValue > 0)
                        Value += MinValue;
                }
                if (_mouseInput.MousePosition.X < Bounds.X + 28)
                {
                    Value = MinValue;
                    var tmp = PointerBounds;
                    tmp.X = Bounds.X + (9*Bounds.Width/200);
                    PointerBounds = tmp;
                }
                if (_mouseInput.MousePosition.X > Bounds.X + Bounds.Width - 28)
                {
                    Value = MaxValue;
                    var tmp = PointerBounds;
                    tmp.X = Bounds.X + Bounds.Width - 38 - (9*Bounds.Width/200);
                    PointerBounds = tmp;
                }
            }

            /*if (drag)
            {
                if (_mouseInput.MousePosition.X > Bounds.X &&
                    _mouseInput.MousePosition.X < Bounds.X + Bounds.Width)
                {
                    Value = (_mouseInput.MousePosition.X - Bounds.X)*((MaxValue-MinValue)/Bounds.Width);
                }
                else if (_mouseInput.MousePosition.X < Bounds.X + Bounds.Width)
                    Value = MinValue;
                else if (_mouseInput.MousePosition.X > Bounds.X)
                    Value = MaxValue;
            }*/

            //Console.WriteLine(ValuePercentage);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void LoadContent()
        {
            Texture = Game.Content.Load<Texture2D>("Textures/UI/Sliderbar");
            Slider = Game.Content.Load<Texture2D>("Textures/UI/Slider");

            base.LoadContent();

            Value = (MaxValue + MinValue)/2;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            spriteBatch.Begin();
            spriteBatch.Draw(Slider, PointerBounds, Color.White);
            spriteBatch.DrawString(Font.MediumFont, (Convert.ToInt32(Value)).ToString(), new Vector2(Bounds.X + Bounds.Width + Margin, Bounds.Y + Font.MediumFont.MeasureString(Value.ToString()).Y/2),Text.Color);
            spriteBatch.End();
        }

        protected virtual void OnValueChanged(EventArgs e)
        {
            EventHandler eh = ValueChanged;
            if (eh != null)
            {
                eh(this, e);
            }
        }
    }

}

