﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Controls.Groups;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls
{
    public class ComboBox : Control
    {
        public int ItemsMargin { get; set; } = 15;
        public Color ColorSelectable { get; set; } = Color.Aquamarine;
        private bool _upsideDown;

        public bool UpsideDown
        {
            get { return _upsideDown; }
            set
            {
                _upsideDown = value;
                SelectableTexture = Game.Content.Load<Texture2D>(value ? "Textures/UI/ComboBoxUpsideDown" : "Textures/UI/ComboBox");
                Texture = Game.Content.Load<Texture2D>(value ? "Textures/UI/ComboBoxUpsideDownArrow" : "Textures/UI/ComboBoxArrow");

                foreach (var comboBoxItem in Items)
                {
                    comboBoxItem.Texture = SelectableTexture;
                }
            }
        }

        private bool isSelected = false;

        private Texture2D _selectableTexture;
        private List<ComboBoxItem> _items;

        public ComboBox(Game game) : base(game)
        {
            Items = new List<ComboBoxItem>();

            BoundsChanged += (sender, args) =>
            {
                Text.Position = new Vector2(x: Bounds.X + ItemsMargin,
                                             y: Bounds.Y + (Bounds.Height / 2) - (Font.MediumFont.MeasureString(Text.Value).Y / 2) + 1);
                Refresh();
            };
        }

        public List<ComboBoxItem> Items
        {
            get { return _items; }
            set
            {
                _items = value;
            }
        }

        public void Refresh()
        {
            int i = 1;
            foreach (var comboBoxItem in _items)
            {
                comboBoxItem.Margin = ItemsMargin;
                if (UpsideDown)
                {
                    comboBoxItem.IsUpsideDown = UpsideDown;
                    comboBoxItem.Bounds = new Rectangle(Bounds.X, Bounds.Y - i * Bounds.Height, Bounds.Width,
                        Bounds.Height);
                }
                else
                    comboBoxItem.Bounds = new Rectangle(Bounds.X, Bounds.Y + i * Bounds.Height, Bounds.Width,
                        Bounds.Height);
                comboBoxItem.Text.Color = ColorSelectable;
                comboBoxItem.Texture = SelectableTexture;
                comboBoxItem.Click += ComboBoxItem_Click;
                comboBoxItem.LoadContent();
                i++;
            }
        }

        public void DeleteChildren(string value)
        {
            if (!string.IsNullOrEmpty(Text.Value))
            {
                var query = Items.Where(x => x.Text.Value.Equals(value));
                Items.Remove(query.FirstOrDefault());

                var index = Convert.ToInt32(value) - 1;

                for (var i = index; i < Items.Count; i++)
                {
                    Items[i].Text.Value = (Convert.ToInt32(Items[i].Text.Value) - 1).ToString();
                }

                if(value.Equals(Text.Value))
                    Text.Value = string.Empty;
                Refresh();
            }
        }

        public void AddChildren(ComboBoxItem cmbItem)
        {
            Items.Add(cmbItem);
            Refresh();
        }

        private void ComboBoxItem_Click(object sender, EventArgs e)
        {
            Text.Value = (sender as ComboBoxItem).Text.Value;
            Text.Position = new Vector2(x: Bounds.X + ItemsMargin,
                             y: Bounds.Y + (Bounds.Height / 2) - (Font.MediumFont.MeasureString(Text.Value).Y / 2) + 1);
            isSelected = false;
        }

        public Texture2D SelectableTexture
        {
            get { return _selectableTexture ?? Game.Content.Load<Texture2D>("Textures/Default"); }
            set { _selectableTexture = value; }
        }

        public void LoadContent()
        {
            Texture = Game.Content.Load<Texture2D>("Textures/UI/ComboBoxArrow");
            SelectableTexture = Game.Content.Load<Texture2D>("Textures/UI/ComboBox");

            base.LoadContent();

            Text.Color = Color.AntiqueWhite;
            Click += (sender, args) => isSelected = !isSelected;
            ClickElseWhere += (sender, args) => isSelected = false;

        }

        public override void Update(GameTime gameTime)
        {
            if (isSelected)
                foreach (ComboBoxItem comboBoxItem in Items)
                    comboBoxItem.Update(gameTime);
            base.Update(gameTime);

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Begin();
            spriteBatch.DrawString(Font.MediumFont, Text.Value, Text.Position, Text.Color);

            spriteBatch.End();

            if (isSelected)
                foreach (ComboBoxItem comboBoxItem in Items)
                    comboBoxItem.Draw(spriteBatch);
        }
    }
}
