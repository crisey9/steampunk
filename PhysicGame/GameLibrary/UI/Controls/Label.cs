﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Map;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls
{
    public class Label : DrawableGameComponent
    {
        private readonly FontsUtil _fonts;
        private Vector2 _measureString;

        public Text Text { get; set; }


        public Vector2 MeasureString
        {
            get
            {
                return new Vector2(_fonts.MediumFont.MeasureString(Text.Value).X, _fonts.MediumFont.MeasureString(Text.Value).Y);
            }
            set { _measureString = value; }
        }


        public Label(Game game) : base(game)
        {
            _fonts = new FontsUtil(game);
            Text = new Text();
        }

        public void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Begin();
            spriteBatch.DrawString(_fonts.MediumFont, Text.Value, Text.Position, Text.Color);
            spriteBatch.End();
        }
    }
}
